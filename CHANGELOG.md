# 2022-10-30

- Added artillery strike event
- Added custom hud (helmet state can now be seen)
- Optimized overall server content file size (addon size has grown but some dependencies have been removed)
