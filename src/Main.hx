import mdg.FireSupport;
import mdg.Utils.LogLevel;
import mdg.Utils.print;
import gmod.libs.VguiLib;
import mdg.SoundScripts.addSoundScripts;
import mdg.macros.Version;
#if client
import mdg.panels.ScoreBoardPlayer;
import mdg.panels.ScoreBoard;
import mdg.panels.MapVote;
#end
import gmod.Gmod;
import mdg.GameHooks;
import mdg.ents.Mdg_Vest;
import mdg.ents.Mdg_Helmet;
import mdg.ents.Mdg_VestHelmet;
import mdg.ents.Mdg_SupplyDrop;
import mdg.ents.Mdg_LootCrate;
import mdg.ents.Mdg_Artillery_Strike;

class Main {
    public static function main() {
        GameHooks.preInit();
        addSoundScripts();
        final version = Version.getVersionName();
        #if client
        print('Masa\'s Deadthgrounds 2 version \'${version}\' - Client init', LogLevel.Ok);
        #end
        #if server
        print('Masa\'s Deadthgrounds 2 version \'${version}\' - Server init', LogLevel.Ok);
        #end
        new GameHooks().setUp();
        FireSupport.init();
    }
}
