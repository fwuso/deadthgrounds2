package mdg;

import gmod.libs.GameLib;
import mdg.ents.Mdg_Artillery_Strike;
import gmod.libs.NetLib;
import gmod.libs.UtilLib;

class FireSupport {
    static final namespace = "fwuso.firesupport";
    public static final net = {
        artilleryFireRound: namespace + ".net.artillery.fireround"
    };

    public static function init() {
        GameLib.AddParticles("particles/explosion_artillery_shell.pcf");
        GameLib.AddDecal("Scorch.Artillery", "decals/artillery/artillery_scorch.vmt");
        #if server
        UtilLib.AddNetworkString(net.artilleryFireRound);
        #end
        #if client
        NetLib.Receive(net.artilleryFireRound, () -> {
            final strike: Mdg_Artillery_Strike = cast NetLib.ReadEntity();
            final strikePos = NetLib.ReadVector();
            strike.fireRound(strikePos);
        });
        #end
    }
}
