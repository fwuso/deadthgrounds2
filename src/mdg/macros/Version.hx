package mdg.macros;

class Version {
    public static macro function getVersionName(): haxe.macro.Expr.ExprOf<String> {
        var versionName = "unknown";
        #if !display
        // Try to parse tag
        var process = new sys.io.Process('git', ['describe', '--exact-match', '--tag', 'HEAD']);
        if (process.exitCode() == 0) {
            versionName = process.stdout.readLine();
        } else {
            // Try to parse commit hash
            process = new sys.io.Process('git', ['rev-parse', 'HEAD']);
            if (process.exitCode() == 0) {
                versionName = process.stdout.readLine();
            }
        }
        #end
        return macro $v{versionName};
    }
}
