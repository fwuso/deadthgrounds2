package mdg.macros;

#if macro
import sys.io.File;
import sys.FileSystem;
import haxe.io.Path;
#end

// Since gmod refuses to include dangerous and mighty JSON files into the addon bundle,
// bake the default missions into the codebase.
class DefaultMissions {
    public static macro function load(): haxe.macro.Expr.ExprOf<Array<Array<String>>> {
        final missions = [];
        #if !display
        final addonPath = haxe.macro.Context.definedValue("gmodAddonFolder");
        final path = Path.join([addonPath, "../data/deadthgrounds2/missions"]);
        final directory = FileSystem.readDirectory(path);
        for (fileName in directory) {
            final mapName = macro $v{Path.withoutExtension(fileName)};
            final mission = macro $v{File.getContent(Path.join([path, fileName]))};
            final a = [mapName, mission];
            missions.push(macro $a{a});
        }
        #end
        return macro $a{missions};
    }
}
