package mdg;

import gmod.Gmod;
import gmod.libs.NetLib;
import gmod.libs.SoundLib;
import gmod.structs.TraceResult;
import gmod.libs.UtilLib;
import gmod.structs.Trace;
import gmod.gclass.Vehicle;
import gmod.gclass.Entity;
import gmod.gclass.Angle;
import gmod.gclass.Vector;

using StringTools;

inline function spawnSimfphys(vehicleName: String, position: Vector, angles: Angle): Null<Entity> {
    // final simfphys:Bool = !(untyped __lua__('simfphys == nil'));
    final vehicle: Entity = untyped __lua__('simfphys.SpawnVehicleSimple({0}, {1}, {2})', vehicleName, position, angles);
    if (vehicle != null) {
        vehicle.SetSkin(Std.random(Math.round(vehicle.SkinCount())));
        untyped __lua__('vehicle:SetActive(false)');
    }
    return vehicle;
}

function prettifyMapName(mapName: String): String {
    var unPrefixed = mapName.substring(mapName.indexOf("_") + 1);
    var parts = unPrefixed.split("_");
    for (pair in parts.keyValueIterator()) {
        var capitalized = pair.value.charAt(0).toUpperCase() + pair.value.substring(1);
        parts[pair.key] = capitalized;
    }
    return parts.join(" ");
}

function traceSky(pos: Vector): Null<Vector> {
    final endPos = new Vector(pos.x, pos.y, pos.z + 65536);
    final tr: Trace = {
        start: pos,
        endpos: endPos,
        mask: MASK_SOLID_BRUSHONLY
    };
    final result = UtilLib.TraceLine(tr);
    return result.HitSky ? result.HitPos : null;
}

function traceDir(pos: Vector, dir: Vector): TraceResult {
    dir = dir.GetNormalized();
    final dist = 93000;
    final endPos = new Vector(pos.x + (dir.x * dist), pos.y + (dir.y * dist), pos.z + (dir.z * dist));
    endPos.Mul(dist);
    final tr: Trace = {
        start: pos,
        endpos: endPos,
        mask: MASK_SOLID
    };
    return UtilLib.TraceLine(tr);
}

enum LogLevel {
    Info;
    Ok;
    Error;
    Warn;
}

function print(message: String, level: LogLevel = LogLevel.Info) {
    #if (!debug && client)
    if (level == LogLevel.Info) {
        return;
    }
    #end
    final spaces = "                                ";
    final message = message.split("\n").join(spaces);

    final ts = Date.now();

    var msgColor = Gmod.Color(255, 255, 255);
    Gmod.MsgC(Gmod.Color(198, 193, 175), ts.toString());
    Gmod.MsgC(Gmod.Color(219, 113, 82), ' MDG2 ');
    Gmod.MsgC(msgColor, '[');
    switch level {
        case Info:
            Gmod.MsgC(Gmod.Color(0, 188, 212), 'INFO');
        case Ok:
            Gmod.MsgC(Gmod.Color(76, 175, 80), ' OK ');
            Gmod.Color(255, 255, 255);
        case Error:
            Gmod.MsgC(Gmod.Color(244, 67, 54), 'FAIL');
        case Warn:
            Gmod.MsgC(Gmod.Color(255, 152, 0), 'WARN');
    }
    Gmod.MsgC(msgColor, '] ${message}\n');
}
