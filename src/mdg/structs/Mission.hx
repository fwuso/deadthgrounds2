package mdg.structs;

typedef Mission = {
    name: Null<String>,
    lobbyspawns: Array<Pos>,
    gamespawns: Array<Pos>,
    dronespawns: Array<Pos>,
    finales: Array<Pos>,
    items: Array<PosAng>,
    vehicles: {
        small: Array<PosAng>, medium: Array<PosAng>, large: Array<PosAng>,
    },
    helicopters: Array<PosAng>,
    supplies: Array<Pos>
}

typedef Pos = {
    pos: Array<Float>
}

typedef PosAng = {
    pos: Array<Float>,
    ang: Array<Float>
}
