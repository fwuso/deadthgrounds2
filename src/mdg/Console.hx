package mdg;

import gmod.Gmod;

class Console {
    public static function createVar(name: String, defaultValue: String, help: String, ?flag: Null<gmod.enums.FCVAR>, ?min: Null<Float>, ?max: Null<Float>) {
        if (!Gmod.ConVarExists(name)) {
            Gmod.CreateConVar(name, defaultValue, flag, help, min, max);
        }
    }

    public static function getVarInt(name: String): Int {
        return Math.floor(Gmod.GetConVar(name).GetInt());
    }

    public static function toBool(value: String): Bool {
        final parsed = Std.parseInt(value);
        return parsed == null ? true : parsed > 0;
    }
}
