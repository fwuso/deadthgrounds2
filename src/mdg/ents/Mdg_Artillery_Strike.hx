package mdg.ents;

import gmod.libs.GameLib;
import gmod.helpers.types.Angle;
import gmod.libs.UtilLib;
import mdg.Utils.traceDir;
import gmod.libs.NetLib;
import gmod.gclass.Entity;
import gmod.libs.SoundLib;
import gmod.libs.TimerLib;
import gmod.gclass.Vector;
import mdg.Utils.traceSky;
import gmod.Gmod;
import gmod.sent.ENT_POINT;
import gmod.helpers.sent.SentBuild;

class Mdg_Artillery_Strike extends SentBuild<ENT_POINT> {
    public var rounds = 32;
    public var delay = 5;
    public var timeSpan = 64;
    public var radius = 2048;

    public var skyPos = new Vector();

    #if server
    private var roundsCounter = 0;
    #end

    static final properties: EntFields = {
        Base: "base_point",
        Spawnable: false,
        PrintName: "Artillery"
    }

    override function UpdateTransmitState(): Float {
        return 0; // TRANSMIT_ALWAYS
    }

    override function Initialize() {
        Gmod.PrecacheParticleSystem("grenade_mortar_explode_large");

        TimerLib.Simple(0.5, () -> {
            if (!Gmod.IsValid(self)) {
                return;
            }
            final tr = traceSky(self.GetPos() + new Vector(0, 0, 4));
            if (tr != null) {
                this.skyPos = tr - new Vector(0, 0, 4);
                TimerLib.Simple(this.delay, () -> {
                    if (Gmod.IsValid(self)) {
                        this.startAttack();
                    }
                });
            }
        });
    }

    function startAttack() {
        #if client
        for (i in(0...this.rounds)) {
            TimerLib.Simple(i * Random.float(0.3, 0.7), () -> {
                if (Gmod.IsValid(self)) {
                    SoundLib.Play('firesupport/artillery/artillery_start${Random.int(1, 2)}.wav', self.GetPos(), 130, Random.float(90, 110), 1);
                }
            });
        }
        #end

        #if server
        for (i in(0...this.rounds)) {
            final mean = (this.timeSpan / this.rounds);
            final delay = (i + 1) * Random.float(mean * 0.8, mean * 1.2);
            TimerLib.Simple(delay, () -> {
                if (!Gmod.IsValid(self)) {
                    return;
                }
                final halfRadius = this.radius / 2;
                final strikePos = this.skyPos + new Vector(Random.float(halfRadius * -1, halfRadius), Random.float(halfRadius * -1, halfRadius), 0);
                this.fireRound(strikePos);
                NetLib.Start(FireSupport.net.artilleryFireRound);
                NetLib.WriteEntity(self);
                NetLib.WriteVector(strikePos);
                NetLib.Broadcast();
            });
        }
        #end
    }

    @:entExpose
    public function fireRound(strikePos: Vector) {
        final tr = traceDir(strikePos, new Vector(0, 0, -1));

        final hitPos = (if (tr.Hit && !tr.StartSolid) tr.HitPos else self.GetPos()) + new Vector(0, 0, 2);
        #if client
        SoundLib.Play('firesupport/artillery/artillery_shell${Random.int(1, 2)}.wav', hitPos + new Vector(0, 0, 128), 80, Random.float(90, 110), 1);
        #end
        TimerLib.Simple(2.7, () -> {
            if (!Gmod.IsValid(self)) {
                return;
            }
            #if client
            SoundLib.Play('firesupport/artillery/artillery_close${Random.int(1, 2)}.wav', hitPos, 100, Random.float(90, 110), 1);
            SoundLib.Play('firesupport/artillery/artillery_dist${Random.int(1, 2)}.wav', hitPos, 140, Random.float(90, 110), 1);
            #end

            #if server
            UtilLib.Decal("Scorch.Artillery", hitPos, hitPos - new Vector(0, 0, 16));
            Gmod.ParticleEffect("grenade_mortar_explode_large", hitPos, new Angle(0, 0, 0));
            UtilLib.ScreenShake(hitPos, 255, 255, 1, 1024);
            final discipliner = if (Gmod.IsValid(self.GetOwner())) self.GetOwner() else GameLib.GetWorld();
            UtilLib.BlastDamage(self, discipliner, hitPos, 512, 300);
            this.roundsCounter += 1;
            if (this.roundsCounter >= this.rounds) {
                Gmod.SafeRemoveEntityDelayed(self, 0.5);
            }
            #end
        });
    }
}
