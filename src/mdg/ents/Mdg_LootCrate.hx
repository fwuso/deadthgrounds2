package mdg.ents;

import gmod.libs.TimerLib;
import gmod.gclass.Player;
import gmod.libs.EntsLib;
import gmod.gclass.Entity;
import gmod.enums.USETYPE;
import gmod.Gmod;
import gmod.sent.ENT_ANIM;
import gmod.helpers.sent.SentBuild;

using mdg.extensions.PlayerExtensions;

class Mdg_LootCrate extends SentBuild<ENT_ANIM> {
    var uncrater: Player = null;
    var uncrateSound = Gmod.Sound('crates/ammo_pack_use.wav');
    final model = Gmod.Model("models/unconid/loot_crate/loot_crate.mdl");

    static final properties: EntFields = {
        Base: "base_anim",
        Spawnable: false
    }

    override function Initialize() {
        #if server
        self.SetModel(model);
        self.SetMoveType(MOVETYPE_VPHYSICS);
        self.SetSolid(SOLID_VPHYSICS);
        self.PhysicsInit(SOLID_VPHYSICS);

        final phys = self.GetPhysicsObject();
        if (Gmod.IsValid(phys)) {
            phys.Wake();
        }
        self.SetUseType(USETYPE.SIMPLE_USE);
        #end
    }

    #if server
    override function Use(activator: Entity, caller: Entity, useType: Float, value: Float) {
        if ((Gmod.IsValid(uncrater) && uncrater.Alive()) || !activator.IsPlayer()) {
            return;
        }
        var player: Player = cast activator;
        uncrater = player;
        player.showHint("Uncrating your loot...");
        self.EmitSound(uncrateSound);
        player.Freeze(true);
        TimerLib.Simple(2, () -> {
            uncrater = null;
            if (player.IsValid()) {
                player.Freeze(false);
                if (player.Alive() && self.IsValid()) {
                    uncrate(player);
                }
            }
        });
    }

    function uncrate(user: Player) {
        var kit = Random.fromArray(kits);
        for (item in kit.items) {
            user.Give(item);
        }
        user.randomHat();
        user.showHint('You uncrated the ${kit.name}');
        var ammo: Entity = EntsLib.Create("cw_ammo_kit_small");
        if (Gmod.IsValid(ammo)) {
            ammo.SetPos(self.GetPos());
            ammo.Spawn();
            ammo.SetCollisionGroup(COLLISION_GROUP_DEBRIS);
        }
        self.Remove();
    }

    static final kits: Array<Kit> = [
        {name: "Sniper Kit", items: ["cw_l115", "cw_makarov", "cw_smoke_grenade", "sg_adrenaline_fix"]},
        {
            name: "Defensive Kit",
            items: [
                "doi_atow_ithaca37",
                "cw_frag_grenade",
                "seal6-claymore",
                "seal6-claymore",
                "sg_adrenaline_fix"
            ]
        },
        {name: "Assault Kit", items: ["cw_famasg2_official", "cw_fiveseven", "cw_flash_grenade", "sg_adrenaline_fix"]},
        {name: "Explosive Kit", items: ["ins2_atow_rpg7", "cw_frag_grenade"]},
        {name: "Garbage Kit", items: ["doi_atow_welrod"]},
        {name: "Wehraboo Kit", items: ["doi_atow_k98", "doi_atow_mp40", "doi_atow_p08"]},
        {name: "British Kit", items: ["doi_atow_lewis", "doi_atow_browninghp"]},
        {name: "Murika Kit", items: ["doi_atow_m1a1", "doi_atow_m1garand"]},
        {name: "Mora Kit", items: ["cw_extrema_ratio_official", "doi_atow_bayonetde", "doi_atow_knifeus"]}
    ];
    #end
}

typedef Kit = {name: String, items: Array<String>};
