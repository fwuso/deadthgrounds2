package mdg.ents;

import gmod.libs.TimerLib;
import gmod.gclass.Player;
import gmod.libs.EntsLib;
import gmod.gclass.Entity;
import lua.Table.AnyTable;
import gmod.gclass.PhysObj;
import gmod.enums.USETYPE;
import gmod.enums.SOLID;
import gmod.enums.MOVETYPE;
import gmod.Gmod;
import gmod.sent.ENT_ANIM;
import gmod.helpers.sent.SentBuild;

using mdg.extensions.PlayerExtensions;

class Mdg_SupplyDrop extends SentBuild<ENT_ANIM> {
    var uncrater: Player = null;
    var uncrateSound = Gmod.Sound('crates/ammo_pack_use.wav');
    var armorSound = Gmod.Sound('items/armor_pickup_0${Random.int(1, 2)}.wav');
    final model = Gmod.Model("models/blacksnow/supplycrate.mdl");
    final chuteModel = Gmod.Model("models/props_survival/paradrop_chute.mdl");
    var chute: Entity = null;

    static final properties: EntFields = {
        Base: "base_anim",
        Spawnable: false
    }

    override function Initialize() {
        #if server
        self.SetModel(model);
        self.SetMoveType(MOVETYPE_VPHYSICS);
        self.SetSolid(SOLID_VPHYSICS);
        self.PhysicsInit(SOLID_VPHYSICS);

        final phys = self.GetPhysicsObject();
        if (Gmod.IsValid(phys)) {
            phys.Wake();
        }
        self.SetUseType(USETYPE.SIMPLE_USE);

        // Set gravity & create parachute
        self.SetGravity(150);
        self.EmitSound("mdgsnd.ambient.parachute");
        chute = EntsLib.Create("prop_dynamic");
        if (Gmod.IsValid(chute)) {
            chute.SetModel(chuteModel);
            chute.SetSolid(SOLID_NONE);
            chute.SetPos(self.GetPos());
            chute.SetParent(self);
            chute.Spawn();
        }
        #end
    }

    #if server
    override function PhysicsCollide(colData: AnyTable, collider: PhysObj) {
        self.SetGravity(600);
        self.StopSound("mdgsnd.ambient.parachute");
        final phys = self.GetPhysicsObject();
        if (Gmod.IsValid(phys)) {
            phys.SetMass(1000);
        }
        if (Gmod.IsValid(chute)) {
            chute.Remove();
        }
    }

    override function Use(activator: Entity, caller: Entity, useType: Float, value: Float) {
        if ((Gmod.IsValid(uncrater) && uncrater.Alive()) || !activator.IsPlayer()) {
            return;
        }
        var player: Player = cast activator;
        uncrater = player;
        player.showHint("Uncrating your loot...");
        self.EmitSound(uncrateSound);
        player.Freeze(true);
        TimerLib.Simple(5, () -> {
            uncrater = null;
            if (player.IsValid()) {
                player.Freeze(false);
                if (player.Alive() && self.IsValid()) {
                    uncrate(player);
                }
            }
        });
    }

    function uncrate(user: Player) {
        var armor = 100;
        var pickedArmor = false;
        var pickedHelmet = false;
        var rest = "";
        if (user.Armor() < armor) {
            user.setBodyArmor(armor);
            var pickedArmor = true;
            self.EmitSound(armorSound);
        }

        if (Random.bool()) {
            pickedHelmet = !user.playerData().hasHelmet;
            user.setHelmet(true);
            user.Give(Random.fromArray(primaries));
            user.Give(Random.fromArray(secondaries));
            rest = "a primary and a secondary weapon";
        } else {
            user.Give("sg_medkit_fix");
            user.Give(Random.fromArray(secondaries));
            rest = "a medkit and a pistol";
        }

        user.showHint('You picked up ${pickedArmor ? "armor, " : ""}${pickedHelmet ? "helmet, " : ""}${rest}.');

        self.Remove();
    }
    #end

    static var primaries = [
        "cw_ak74", "cw_ar15", "cw_famasg2_official", "cw_scarh", "cw_g3a3", "cw_g36c", "cw_l85a2", "cw_m14", "cw_xm1014_official", "cw_saiga12k_official",
        "cw_svd_official", "cw_vss", "cw_shorty", "cw_mp9_official", "cw_mac11", "cw_m3super90", "cw_ump45", "cw_mp5", "cw_mp7_official"
    ];
    static var secondaries = [
        "cw_makarov",
        "cw_p99",
        "cw_mr96",
        "doi_atow_m1911a1",
        "cw_deagle",
        "cw_fiveseven"
    ];
}
