package mdg.ents;

import gmod.libs.EntsLib;
import gmod.structs.TraceResult;
import gmod.gclass.Player;
import gmod.gclass.Entity;
import gmod.enums.USETYPE;
import gmod.Gmod;
import gmod.enums.SOLID;
import gmod.enums.MOVETYPE;
import gmod.sent.ENT_ANIM;
import gmod.helpers.sent.SentBuild;

using mdg.extensions.PlayerExtensions;

class Mdg_VestHelmet extends SentBuild<ENT_ANIM> {
    #if server
    public var model = Gmod.Model("models/props_survival/upgrades/upgrade_dz_armor_helmet.mdl");
    public var sound = Gmod.Sound('items/armor_pickup_0${Random.int(1, 2)}.wav');
    public var armor = 100;
    #end

    static final properties: EntFields = {
        Base: "base_anim",
        Spawnable: false
    }

    override function Initialize() {
        #if server
        self.SetModel(model);
        self.SetMoveType(MOVETYPE.MOVETYPE_VPHYSICS);
        self.SetSolid(SOLID.SOLID_VPHYSICS);
        self.PhysicsInit(SOLID_VPHYSICS);

        final phys = self.GetPhysicsObject();
        if (Gmod.IsValid(phys)) {
            phys.Wake();
        }
        self.SetUseType(USETYPE.SIMPLE_USE);
        #end
    }

    #if server
    function SpawnFunction(player: Player, tr: TraceResult, className: String): Entity {
        if (!tr.Hit) {
            return null;
        }

        var spawnPos = tr.HitPos + tr.HitNormal * 10;
        var spawnAng = player.EyeAngles();
        spawnAng.p = 0;
        spawnAng.y = spawnAng.y + 180;

        var ent = EntsLib.Create(className);
        ent.SetPos(spawnPos);
        ent.SetAngles(spawnAng);
        ent.Spawn();
        ent.Activate();

        return ent;
    }

    override function Use(activator: Entity, caller: Entity, useType: Float, value: Float) {
        if (!activator.IsPlayer()) {
            return;
        }
        final activator: Player = cast activator;
        pickUp(activator);
    }

    public function pickUp(player: Player) {
        if (player.Armor() >= armor && player.playerData().hasHelmet) {
            player.showHint("You're already wearing better armor and a helmet.");
            return;
        }
        player.setBodyArmor(armor);
        player.setHelmet(true);
        if (sound != null) {
            self.EmitSound(sound);
        }
        self.Remove();
    }
    #end
}
