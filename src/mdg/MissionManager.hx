package mdg;

import mdg.macros.DefaultMissions;
import mdg.Utils.LogLevel;
import mdg.Utils.print;
import mdg.structs.Mission;
import haxe.Json;
import haxe.Exception;
import gmod.libs.GameLib;
import lua.Table;
import gmod.libs.FileLib;

class MissionManager {
    final missionDir: String;
    final defaults: Map<String, String> = [];

    public function new(missionDir: String) {
        if (missionDir.charAt(missionDir.length - 1) != "/") {
            missionDir = '${missionDir}/';
        }
        this.missionDir = missionDir;
    }

    public function init(): Array<String> {
        final missionMaps = Table.toArray(FileLib.Find('${missionDir}*.json', Path.DATA).files);
        for (m in missionMaps.keyValueIterator()) {
            missionMaps[m.key] = m.value.substring(0, m.value.length - 5);
        }
        final defaultMissions = DefaultMissions.load();
        for (dm in defaultMissions) {
            final mapName = dm[0];
            final missionFile = dm[1];
            // Local file overrides default mission
            if (!missionMaps.contains(mapName)) {
                defaults.set(mapName, missionFile);
            }
        }
        final defaultMaps = [];
        for (k in defaults.keys()) {
            defaultMaps.push(k);
        }
        print('Added default missions to collection: ${defaultMaps.length > 0 ? defaultMaps.join(", ") : "*none*"}', LogLevel.Info);
        return missionMaps.concat(defaultMaps);
    }

    public function getMapMission(mapName: String): Null<Mission> {
        print("Loading mission file...");
        final fileName = '${missionDir}${mapName}.json';
        final defaultMap = defaults.get(mapName);
        final json = defaultMap == null ? FileLib.Read(fileName, Path.DATA) : defaultMap;
        var missionData: Mission = null;
        try {
            if (json == null) {
                throw new Exception('File \'data/${fileName}\' does not exist.');
            }
            missionData = Json.parse(json);
        } catch (e) {
            print('Error reading mission file: ${e.message}', LogLevel.Error);
        }
        if (missionData != null) {
            print("Mission file is OK");
        }
        return missionData;
    }
}
