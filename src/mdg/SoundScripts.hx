package mdg;

import gmod.enums.SNDLVL;
import gmod.libs.SoundLib;

function addSoundScripts() {
    SoundLib.Add({
        name: "mdgsnd.hit.helmet",
        pitch: 100,
        sound: "player/hit_helmet.wav",
        level: SNDLVL_85dB,
        volume: 1.0,
        channel: CHAN_BODY,
        pitchstart: 100,
        pitchend: 100
    });
    SoundLib.Add({
        name: "mdgsnd.ambient.parachute",
        pitch: 100,
        sound: "crates/ammo_crate_parachute_flutter_loop.wav",
        level: SNDLVL_90dB,
        volume: 1.0,
        channel: CHAN_AUTO,
        pitchstart: 100,
        pitchend: 100
    });

    final plrMap = [
        "deadth1" => {level: SNDLVL_75dB},
        "deadth2" => {level: SNDLVL_75dB},
        "deadth3" => {level: SNDLVL_80dB},
        "deadth4" => {level: SNDLVL_90dB},
        "deadth5" => {level: SNDLVL_120dB},
    ];

    for (k => v in plrMap) {
        SoundLib.Add({
            name: "mdgsnd.player." + k,
            pitch: 100,
            sound: 'player/${k}.wav',
            level: v.level,
            volume: 1.0,
            channel: CHAN_AUTO,
            pitchstart: 90,
            pitchend: 110
        });
    }
}
