package mdg.panels;

import mdg.Utils.prettifyMapName;
import gmod.libs.EngineLib;
import gmod.libs.GameLib;
#if client
import gmod.helpers.GLinked;
import gmod.panels.DScrollPanel;
import gmod.libs.PlayerLib;
import gmod.panels.DLabel;
import gmod.Gmod;
import gmod.enums.DOCK;
import gmod.libs.VguiLib;
import gmod.stringtypes.PanelClass.GMPanels;
import gmod.panels.DPanel;
import gmod.panels.EditablePanel;
import gmod.helpers.panel.PanelBuild;

using mdg.extensions.PlayerExtensions;

typedef GScoreBoard = GLinked<EditablePanel, ScoreBoard>;

@:expose("ScoreBoard")
class ScoreBoard extends PanelBuild<gmod.panels.EditablePanel> {
    var serverLabel: DLabel = null;
    var playerCountLabel: DLabel = null;
    var roundsRemaining: DLabel = null;
    var list: DScrollPanel = null;
    var rounds = -1;
    var players = -1;
    var maxPlayers = -1;
    var serverName = "";

    static var instance = null;

    public static function getInstance(): GScoreBoard {
        if (instance == null) {
            instance = VguiLib.Create(gclass);
        }
        return instance;
    }

    override function Init(): Void {
        final header = VguiLib.Create(GMPanels.DPanel);
        self.Add(header);
        header.Dock(DOCK.TOP);
        header.SetHeight(128);
        header.SetPaintBackground(false);

        final titleHolder = VguiLib.Create(GMPanels.DPanel);
        header.Add(titleHolder);
        titleHolder.Dock(DOCK.TOP);
        titleHolder.SetPaintBackground(false);
        titleHolder.SetSize(700, 40);

        final title = VguiLib.Create(GMPanels.DLabel);
        titleHolder.Add(title);
        title.SetFont(Theme.titleFontLarge);
        title.SetTextColor(Theme.textColor);
        title.SetExpensiveShadow(2, Theme.textShadow);
        title.SetText(EngineLib.ActiveGamemode());
        title.Dock(LEFT);
        title.SizeToContentsX();

        final titleDivider = VguiLib.Create(GMPanels.DPanel);
        titleHolder.Add(titleDivider);
        titleDivider.SetWidth(2);
        titleDivider.SetBackgroundColor(Theme.textColor.ToTable());
        titleDivider.DockMargin(Theme.scoreBoardPadding, 0, Theme.scoreBoardPadding, 0);
        titleDivider.Dock(LEFT);

        final map = VguiLib.Create(GMPanels.DLabel);
        titleHolder.Add(map);
        map.SetFont(Theme.textFontLarge);
        map.SetTextColor(Theme.textColor);
        map.SetExpensiveShadow(2, Theme.textShadow);
        map.SetText(prettifyMapName(GameLib.GetMap()));
        map.Dock(LEFT);
        map.SizeToContentsX();

        final playerStatBar = VguiLib.Create(GMPanels.DPanel);
        header.Add(playerStatBar);
        playerStatBar.Dock(DOCK.BOTTOM);
        playerStatBar.SetPaintBackground(false);
        // Padding for mute button
        playerStatBar.DockPadding(0, 0, 32 + Theme.scoreBoardMargin, 0);

        for (i in ["Ping", "Deaths", "Kills", "Wins"]) {
            final label = VguiLib.Create(GMPanels.DLabel);
            playerStatBar.Add(label);
            label.Dock(DOCK.RIGHT);
            label.SetWidth(64);
            label.SetContentAlignment(5);
            label.SetFont(Theme.titleFontSmall);
            label.SetTextColor(Theme.textColor);
            label.DockMargin(Theme.scoreBoardMargin, 0, 0, 0);
            label.SetText(i);
        }

        final infobar = VguiLib.Create(GMPanels.DPanel);
        header.Add(infobar);
        infobar.SetHeight(52);
        infobar.Dock(DOCK.BOTTOM);
        // infobar.SetHeight(64);
        infobar.SetPaintBackground(false);

        final roundsStat = createServerLabel("Rounds Remaining");
        infobar.Add(roundsStat.panel);
        roundsStat.panel.Dock(LEFT);
        roundsRemaining = roundsStat.label;

        final playerStat = createServerLabel("Players");
        infobar.Add(playerStat.panel);
        playerStat.panel.Dock(RIGHT);
        playerCountLabel = playerStat.label;

        final nameStat = createServerLabel("Server Name");
        infobar.Add(nameStat.panel);
        nameStat.panel.Dock(RIGHT);
        serverLabel = nameStat.label;

        list = VguiLib.Create(GMPanels.DScrollPanel);
        self.Add(list);
        list.Dock(DOCK.FILL);
    }

    override function PerformLayout(width: Float, height: Float): Void {
        final w = 700;
        self.SetSize(w, Gmod.ScrH() - 200);
        self.SetPos((Gmod.ScrW() / 2) - (w / 2), 100);
    }

    override function Paint(width: Float, height: Float): Bool {
        return false;
    }

    override function Think(): Void {
        if (serverName != Gmod.GetHostName()) {
            serverName = Gmod.GetHostName();
            serverLabel.SetText(serverName);
        }

        if (rounds != Gmod.GetGlobalInt("mdg.globals.roundsplayed")) {
            rounds = Math.round(Gmod.GetGlobalInt("mdg.globals.roundsplayed"));
            var remaining = Console.getVarInt("mdg_maxrounds") - rounds;
            if (remaining < 0) {
                remaining = 0;
            }
            roundsRemaining.SetText('${remaining}');
        }

        if (players != PlayerLib.GetCount() || maxPlayers != GameLib.MaxPlayers()) {
            players = PlayerLib.GetCount();
            maxPlayers = Math.round(GameLib.MaxPlayers());
            playerCountLabel.SetText('$players/$maxPlayers');
        }

        for (player in PlayerLib.GetAll()) {
            if (Gmod.IsValid(player)) {
                if (player.playerData().scoreBoardEntry != null) {
                    continue;
                }
            }

            final entry = VguiLib.Create(ScoreBoardPlayer.gclass);
            player.playerData().scoreBoardEntry = entry;
            player.playerData().scoreBoardEntry.setUp(player);
            list.AddItem(entry);
        }
    }

    function createServerLabel(title: String): {panel: DPanel, label: DLabel} {
        final panel = VguiLib.Create(GMPanels.DPanel);

        final header = VguiLib.Create(GMPanels.DLabel);
        panel.Add(header);
        header.Dock(DOCK.TOP);
        header.SetContentAlignment(4);
        header.SetFont(Theme.titleFontSmall);
        header.SetTextColor(Theme.textColor);
        header.SetText(title);
        header.SizeToContentsX();

        final label = VguiLib.Create(GMPanels.DLabel);
        panel.Add(label);
        label.Dock(DOCK.TOP);
        label.SetContentAlignment(4);
        label.SetFont(Theme.textFontSmall);
        label.SetTextColor(Theme.textColor);

        panel.DockMargin(Theme.scoreBoardMargin, 0, Theme.scoreBoardMargin, 0);
        panel.SetPaintBackground(false);

        panel.SizeToChildren(true, true);
        panel.InvalidateChildren();
        return {panel: panel, label: label};
    }
}
#end
