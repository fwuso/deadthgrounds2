package mdg.panels;

import gmod.panels.DIconLayout;
import gmod.panels.DGrid;
import gmod.libs.DrawLib;
import gmod.panels.DButton;
#if client
import gmod.libs.FileLib;
import mdg.Utils.prettifyMapName;
import gmod.panels.DLabel;
import gmod.Gmod;
import gmod.helpers.GLinked;
import gmod.enums.DOCK;
import gmod.stringtypes.PanelClass.GMPanels;
import gmod.libs.VguiLib;
import mdg.panels.ScoreBoard;
import gmod.panels.EditablePanel;
import gmod.panels.DPanel;
import gmod.helpers.panel.PanelBuild;

typedef GMapVote = GLinked<EditablePanel, MapVote>;

@:expose("MapVote")
class MapVote extends PanelBuild<gmod.panels.EditablePanel> {
    static var instance = null;

    public static function getInstance(): GMapVote {
        if (instance == null) {
            instance = VguiLib.Create(gclass);
        }
        return instance;
    }

    var mapGrid: DIconLayout = null;
    var title: DLabel = null;
    var ownVote: String = "";
    var labels: Map<String, {panel: DPanel, label: DLabel}> = [];
    var votes: Map<String, Int> = [];
    var voteEndTime = 0.0;
    var timerText = "";
    var timerSeconds = 0;
    var timerLabel: DLabel = null;

    public function setUp(maps: Array<String>, voteEndTime: Float) {
        this.votes = [];
        this.timerSeconds = 0;

        this.voteEndTime = voteEndTime;
        timerText = "Voting Ends In";

        for (mapName in maps) {
            mapGrid.Add(createMapPanel(mapName));
        }
    }

    public function updateVote(oldMap: Null<String>, newMap: String) {
        if (oldMap != null && votes.exists(oldMap)) {
            var oldVotes = votes.get(oldMap);
            var newVotes = oldVotes != null && oldVotes > 0 ? oldVotes - 1 : 0;
            votes.set(oldMap, newVotes);
            var l = labels.get(oldMap);
            if (l != null) {
                if (newVotes > 0) {
                    l.panel.Show();
                } else {
                    l.panel.Hide();
                }
                l.label.SetText('$newVotes');
            }
        }
        var oldVotes = votes.get(newMap);
        var newVotes = oldVotes != null ? oldVotes + 1 : 1;
        votes.set(newMap, newVotes);
        var l = labels.get(newMap);
        if (l != null) {
            l.panel.Show();
            l.label.SetText('$newVotes');
        }
    }

    public function voteEnd(chosenMap: String, changeTime: Float) {
        timerText = "Map Changes In";
        voteEndTime = changeTime;
        if (title != null) {
            title.SetText('Next map: ${prettifyMapName(chosenMap)}');
            title.SizeToContentsX();
        }
    }

    override function Init(): Void {
        self.ParentToHUD();
        self.MakePopup();
        self.SetKeyboardInputEnabled(false);

        final header = VguiLib.Create(DPanel);
        self.Add(header);
        header.Dock(TOP);
        header.SetHeight(128);
        header.SetPaintBackground(false);

        title = VguiLib.Create(DLabel);
        header.Add(title);
        title.SetFont(Theme.titleFontLarge);
        title.SetTextColor(Theme.textColor);
        title.SetExpensiveShadow(2, Theme.textShadow);
        title.SetText("Choose the next map");
        title.Dock(LEFT);
        title.SizeToContentsX();

        timerLabel = VguiLib.Create(DLabel);
        header.Add(timerLabel);
        timerLabel.SetFont(Theme.textFontSmall);
        timerLabel.SetTextColor(Theme.textColor);

        final scrollable = VguiLib.Create(DScrollPanel);
        self.Add(scrollable);

        scrollable.Dock(FILL);
        mapGrid = VguiLib.Create(DIconLayout, scrollable);
        mapGrid.SetSpaceX(Theme.scoreBoardPadding);
        mapGrid.SetSpaceY(Theme.scoreBoardPadding);
        mapGrid.Dock(FILL);
        mapGrid.SetPaintBackground(false);
    }

    override function PerformLayout(width: Float, height: Float): Void {
        final w = 740;
        self.SetSize(w, Gmod.ScrH() - 200);
        self.SetPos((Gmod.ScrW() / 2) - (w / 2), 100);
    }

    override function Think(): Void {
        var curTime = Gmod.CurTime();
        var endTimer = Math.round(Math.max(voteEndTime - curTime, 0));
        if (timerSeconds != endTimer) {
            timerSeconds = endTimer;
            var minutes = Math.floor(timerSeconds / 60);
            var seconds = timerSeconds % 60;
            timerLabel.SetText('$minutes:${seconds > 9 ? '$seconds' : '0' + seconds}');
        }
    }

    function createMapPanel(mapName: String): DButton {
        var prettyName = prettifyMapName(mapName);
        var btn = VguiLib.Create(DButton);
        var label = VguiLib.Create(DLabel, btn);
        var votePanel = VguiLib.Create(DPanel, btn);
        var voteLabel = VguiLib.Create(DLabel, votePanel);
        /*
            var fgPanel = VguiLib.Create(DPanel, btn);
         */

        btn.SetSize(240, 40);
        label.SetMouseInputEnabled(false);

        btn.SetText("");
        btn.SetPaintBackgroundEnabled(false);
        untyped __lua__('btn.Paint = function(s, w, h) self:paintItem(w, h, false) end');
        untyped __lua__('votePanel.Paint = function(s, w, h) self:paintLabel(w, h, mapName) end');

        label.SetFont(Theme.textFontSmall);
        label.SetTextColor(Theme.textColor);
        label.SetText(prettyName);
        label.Dock(LEFT);
        label.DockMargin(Theme.scoreBoardPadding, 0, 0, 0);
        label.SetSize(190, 40);
        voteLabel.SetContentAlignment(4);

        votePanel.Dock(RIGHT);
        votePanel.DockMargin(0, Theme.scoreBoardPadding, Theme.scoreBoardPadding, Theme.scoreBoardPadding);
        votePanel.SetSize(32, 32);

        voteLabel.SetFont(Theme.textFontSmall);
        voteLabel.SetTextColor(Theme.textColor);
        voteLabel.SetText("0");
        voteLabel.SetSize(32, 32);
        voteLabel.SetContentAlignment(5);

        untyped __lua__('btn.DoClick = function() self:clickMap(mapName) end');
        labels.set(mapName, {label: voteLabel, panel: votePanel});

        votePanel.Hide();
        return btn;
    }

    @:keep
    function clickMap(mapName: String) {
        ownVote = mapName;
        MapVoteManager.vote(mapName);
    }

    @:keep
    function paintItem(width: Float, height: Float, flicker: Bool) {
        if (flicker) {
            DrawLib.RoundedBox(Theme.cornerRadius, 0, 0, width, height, Gmod.Color(100, 100, 100, 255));
        } else {
            DrawLib.RoundedBox(Theme.cornerRadius, 0, 0, width, height, Theme.notificationColor);
        }
        return false;
    }

    @:keep
    function paintLabel(width: Float, height: Float, map: String) {
        if (ownVote == map) {
            DrawLib.RoundedBox(Theme.cornerRadius, 0, 0, width, height, Gmod.Color(40, 150, 40, 255));
        } else {
            DrawLib.RoundedBox(Theme.cornerRadius, 0, 0, width, height, Gmod.Color(50, 50, 50, 255));
        }
        return false;
    }
}
#end
