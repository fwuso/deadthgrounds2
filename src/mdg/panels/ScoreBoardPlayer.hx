package mdg.panels;

import mdg.GameHooks.MDG_SPECTATORS;
import mdg.GameHooks.MDG_PLAYERS;
import mdg.GameHooks.MDG_NONE;
#if client
import gmod.panels.DPanel;
import gmod.libs.DrawLib;
import gmod.Gmod;
import gmod.gclass.Player;
import gmod.panels.DImageButton;
import gmod.panels.DLabel;
import gmod.enums.TEXT_ALIGN;
import gmod.stringtypes.Hook.GMHook;
import gmod.enums.DOCK;
import gmod.libs.VguiLib;
import gmod.stringtypes.PanelClass.GMPanels;
import gmod.gclass.Panel;
import gmod.panels.DButton;
import gmod.panels.AvatarImage;
import gmod.helpers.panel.PanelBuild;

using mdg.extensions.PlayerExtensions;

class ScoreBoardPlayer extends PanelBuild<gmod.panels.DPanel> {
    var player: Player = null;
    var avatar: AvatarImage = null;
    var avatarButton: DButton = null;
    var nameLabel: DLabel = null;
    var mute: DImageButton = null;
    var ping: DLabel = null;
    var deaths: DLabel = null;
    var kills: DLabel = null;
    var wins: DLabel = null;

    var extraText = "";
    var name = "";
    var numWins = -1;
    var numKills = -1;
    var numDeaths = -1;
    var numPing = -1;
    var isMuted = false;

    public function setUp(player: Player) {
        this.player = player;
        avatar.SetPlayer(player, 32);
    }

    override function Init(): Void {
        avatarButton = VguiLib.Create(GMPanels.DButton);
        avatar = VguiLib.Create(GMPanels.AvatarImage, avatarButton);
        self.Add(avatarButton);
        avatarButton.Dock(DOCK.LEFT);
        avatarButton.SetSize(32, 32);
        untyped __lua__('self.avatarButton.DoClick = function() self.player:ShowProfile() end');
        avatar.SetSize(32, 32);
        avatar.SetMouseInputEnabled(false);

        nameLabel = VguiLib.Create(GMPanels.DLabel);
        self.Add(nameLabel);
        nameLabel.Dock(DOCK.FILL);
        nameLabel.SetFont(Theme.textFontSmall);
        nameLabel.SetTextColor(Theme.textColor);
        nameLabel.DockMargin(Theme.scoreBoardMargin, 0, 0, 0);

        mute = VguiLib.Create(GMPanels.DImageButton);
        self.Add(mute);
        mute.Dock(DOCK.RIGHT);
        mute.SetSize(32, 32);

        ping = createStatLabel();
        self.Add(ping);

        deaths = createStatLabel();
        self.Add(deaths);

        kills = createStatLabel();
        self.Add(kills);

        wins = createStatLabel();
        self.Add(wins);

        self.Dock(DOCK.TOP);
        self.SetHeight(32);
        self.DockMargin(Theme.scoreBoardMargin, 0, Theme.scoreBoardMargin, Theme.scoreBoardMargin);
    }

    override function Paint(width: Float, height: Float): Bool {
        if (!Gmod.IsValid(player)) {
            return false;
        }

        if (player.Team() == TEAM_CONNECTING) {
            DrawLib.RoundedBox(Theme.cornerRadius, 0, 0, width, height, Gmod.Color(50, 50, 50, 50));
            return false;
        }

        if (player.Team() == 2) {
            DrawLib.RoundedBox(Theme.cornerRadius, 0, 0, width, height, Gmod.Color(90, 25, 25, 200));
            return false;
        }

        DrawLib.RoundedBox(Theme.cornerRadius, 0, 0, width, height, Theme.notificationColor);
        return false;
    }

    override function Think(): Void {
        if (!Gmod.IsValid(player)) {
            self.SetZPos(9999);
            self.Remove();
            return;
        }

        var team = player.Team();

        if (team == TEAM_CONNECTING && extraText != "(CONNECTING) ") {
            extraText = "(CONNECTING) ";
            nameLabel.SetText(extraText + name);
        }

        if (team == MDG_SPECTATORS && extraText != "(DEAD) ") {
            extraText = "(DEAD) ";
            nameLabel.SetText(extraText + name);
        }

        if (team == MDG_PLAYERS || team == MDG_NONE && extraText != "") {
            extraText = "";
            nameLabel.SetText(extraText + name);
        }

        if (name != player.Nick()) {
            name = player.Nick();
            nameLabel.SetText(extraText + name);
        }

        if (numKills != player.Frags()) {
            numKills = Math.round(player.Frags());
            kills.SetText('${numKills}');
        }

        if (numDeaths != player.Deaths()) {
            numDeaths = Math.round(player.Deaths());
            deaths.SetText('${numDeaths}');
        }

        if (numPing != player.Ping()) {
            numPing = Math.round(player.Ping());
            ping.SetText('${numPing}');
        }

        if (numWins != player.getWins()) {
            numWins = player.getWins();
            wins.SetText('${numWins}');
        }

        if (isMuted != player.IsMuted()) {
            isMuted = player.IsMuted();
            if (isMuted) {
                mute.SetImage("icon32/muted.png", "Muted");
            } else {
                mute.SetImage("icon32/unmuted.png", "Unmuted");
            }
        }

        var sortingScore = (numWins * -100) + (numKills * -10) + numDeaths;

        if (player.Team() == TEAM_CONNECTING) {
            self.SetZPos(2000);
            return;
        }
        if (player.Team() == 2) { // TEAM_SPECTATORS
            self.SetZPos(1000);
            return;
        }
        self.SetZPos(sortingScore);
    }

    function createStatLabel() {
        final label = VguiLib.Create(GMPanels.DLabel);
        label.Dock(DOCK.RIGHT);
        label.SetWidth(64);
        label.SetContentAlignment(5);
        label.SetFont(Theme.textFontSmall);
        label.SetTextColor(Theme.textColor);
        label.DockMargin(Theme.scoreBoardMargin, 0, 0, 0);
        return label;
    }
}
#end
