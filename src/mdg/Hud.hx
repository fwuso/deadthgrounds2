package mdg;

import gmod.libs.SurfaceLib;
import gmod.structs.Color;
import gmod.libs.DrawLib;
import gmod.libs.MathLib;
import gmod.libs.CvarsLib;
import gmod.Gmod;
import gmod.libs.GameLib;
import gmod.libs.UtilLib;

using mdg.extensions.PlayerExtensions;

class Hud {
    #if client
    static final icons = {
        health: Gmod.Material("materials/mdg/hud/cross.png").a,
        armour: Gmod.Material("materials/mdg/hud/armour.png").a,
        ammo: Gmod.Material("materials/mdg/hud/ammo.png").a,
        grenade: Gmod.Material("materials/mdg/hud/grenade.png").a,
        armour_helmet: Gmod.Material("materials/mdg/hud/armour_helmet.png").a,
        helmet: Gmod.Material("materials/mdg/hud/helmet.png").a
    };

    static var drawHud = false;
    static var hudColour: Color = {
        r: 255,
        g: 255, // 216,
        b: 255, // 0,
        a: 250
    };
    static var hudGray: Color = {
        r: Theme.notificationColor.r,
        g: Theme.notificationColor.g,
        b: Theme.notificationColor.b,
        a: 128,
    };
    static var hudDanger: Color = {
        r: Theme.dangerColor.r,
        g: Theme.dangerColor.g,
        b: Theme.dangerColor.b,
        a: 250,
    };
    static var margin = Theme.scoreBoardMargin;
    static var padding = Theme.scoreBoardPadding;
    static var corner = Theme.cornerRadius;
    static var barWidth = 10;
    static var barHeight = 32;
    static var iconSize = 32;
    #end

    public static function setUp() {
        #if client
        CvarsLib.AddChangeCallback("cl_drawhud", (name: String, old: String, current: String) -> {
            drawHud = Console.toBool(current);
        });
        #end
    }

    #if client
    inline static function paintBar(x: Float, y: Float, color: Color) {
        DrawLib.RoundedBox(corner, x, y, barWidth, barHeight, color);
    }

    public static function paint() {
        final player = Gmod.LocalPlayer();
        if (!player.Alive()) {
            return;
        }

        final screenWidth = Gmod.ScrW();
        final screenHeight = Gmod.ScrH();

        // Left side: health/armor indicators

        final health = player.Health();
        final maxHealth = player.GetMaxHealth();
        final healthBars = Std.int(MathLib.min(MathLib.ceil((health / maxHealth) * 10)));
        final hasOverheal = health > maxHealth;
        final stackHeight = barHeight + margin;
        final iconStackWidth = margin + iconSize;
        final healthColor = healthBars > 2 ? hudColour : hudDanger;

        SurfaceLib.SetDrawColor(healthColor);
        SurfaceLib.SetMaterial(icons.health);
        SurfaceLib.DrawTexturedRect(margin, screenHeight - stackHeight, iconSize, iconSize);

        for (i in(0...10)) {
            final color = i < healthBars ? healthColor : hudGray;
            paintBar((i * (barWidth + padding)) + margin + iconStackWidth, screenHeight - stackHeight, color);
        }

        final armor = player.Armor();
        final maxArmor: Float = untyped __lua__('player:GetMaxArmor()');
        final armorBars = Std.int(MathLib.min(MathLib.ceil((armor / maxArmor) * 10)));
        final hasHelmet = player.playerData().hasHelmet;
        final hasArmor = armor > 1;

        final armorIcon = hasHelmet && hasArmor ? icons.armour_helmet : (hasHelmet ? icons.helmet : (hasArmor ? icons.armour : null));

        if (armorIcon != null) {
            SurfaceLib.SetDrawColor(hudColour);
            SurfaceLib.SetMaterial(armorIcon);
            SurfaceLib.DrawTexturedRect(margin, screenHeight - (stackHeight * 2), iconSize, iconSize);
        }

        if (hasArmor) {
            for (i in(0...10)) {
                final color = i < armorBars ? hudColour : hudGray;
                paintBar((i * (barWidth + padding)) + margin + iconStackWidth, screenHeight - (stackHeight * 2), color);
            }
        }

        // Right side: weapon/ammo indicators

        final weapon = player.GetActiveWeapon();

        if (Gmod.IsValid(weapon) && !player.InVehicle()) {
            final magazine = weapon.Clip1();
            final reserve = player.GetAmmoCount(weapon.GetPrimaryAmmoType());
            final secondaryAmmo = player.GetAmmoCount(weapon.GetSecondaryAmmoType());

            final horizontalAnchor = screenWidth - 64;
            final verticalAnchor = screenHeight - margin;
            final fontHeight = 36;

            if (magazine >= 0 || reserve > 0) {
                DrawLib.SimpleText('${magazine}', Theme.textFontLarge, horizontalAnchor - margin, verticalAnchor, hudColour, TEXT_ALIGN_RIGHT,
                    TEXT_ALIGN_BOTTOM);
                DrawLib.SimpleText('/', Theme.textFontLarge, horizontalAnchor, verticalAnchor, hudColour, TEXT_ALIGN_CENTER, TEXT_ALIGN_BOTTOM);
                DrawLib.SimpleText('${reserve}', Theme.textFontSmall, horizontalAnchor + margin, verticalAnchor, hudColour, TEXT_ALIGN_LEFT,
                    TEXT_ALIGN_BOTTOM);
            }

            if (secondaryAmmo > 0) {
                DrawLib.SimpleText('${secondaryAmmo}', Theme.textFontLarge, screenWidth - margin, verticalAnchor - fontHeight, hudColour, TEXT_ALIGN_RIGHT,
                    TEXT_ALIGN_BOTTOM);
            }
        }
    }
    #end
}
