package mdg;

import mdg.panels.MapVote;
import lua.lib.luv.Timer;
import gmod.libs.TimerLib;
import lua.Table;
import gmod.libs.FileLib;
import gmod.gclass.Player;
import gmod.Gmod;
import gmod.libs.NetLib;
import gmod.libs.UtilLib;

class MapVoteManager {
    static final votes = new Map<Player, String>();

    #if server
    static var canVote = false;
    #end

    public static function setUp() {
        #if server
        UtilLib.AddNetworkString("mdg.net.mapvote.start");
        UtilLib.AddNetworkString("mdg.net.mapvote.end");
        UtilLib.AddNetworkString("mdg.net.mapvote.cancel");
        UtilLib.AddNetworkString("mdg.net.mapvote.playervote");

        NetLib.Receive("mdg.net.mapvote.playervote", (length, player) -> {
            final map = NetLib.ReadString();
            if (canVote) {
                receiveVote(player, map);
            }
        });
        #end

        #if client
        NetLib.Receive("mdg.net.mapvote.start", (length, player) -> {
            final voteEndTime = NetLib.ReadFloat();
            final mapAmount = NetLib.ReadInt(32);
            final mapList: Array<String> = [];
            for (_ in 0...mapAmount) {
                final map = NetLib.ReadString();
                mapList.push(map);
            }
            // Create panel
            var votePanel = MapVote.getInstance();
            var hxPanel = votePanel.toHaxe();
            hxPanel.setUp(mapList, voteEndTime);
            votePanel.Show();
            votePanel.MakePopup();
        });

        NetLib.Receive("mdg.net.mapvote.end", (length, player) -> {
            final changeTime = NetLib.ReadFloat();
            final chosenMap = NetLib.ReadString();

            var votePanel = MapVote.getInstance().toHaxe();
            votePanel.voteEnd(chosenMap, changeTime);
        });

        NetLib.Receive("mdg.net.mapvote.cancel", (length, player) -> {
            // Remove panel if exists
            var gvotePanel: GMapVote = cast MapVote.getInstance();
            gvotePanel.Hide();
        });

        NetLib.Receive("mdg.net.mapvote.playervote", (length, player) -> {
            final plr: Player = cast NetLib.ReadEntity();
            final newMap = NetLib.ReadString();
            final oldMap = votes.get(plr);
            votes.set(plr, newMap);

            var votePanel = MapVote.getInstance().toHaxe();
            votePanel.updateVote(oldMap, newMap);
        });
        #end
    }

    #if client
    public static function vote(mapName: String) {
        NetLib.Start("mdg.net.mapvote.playervote");
        NetLib.WriteString(mapName);
        NetLib.SendToServer();
    }
    #end

    #if server
    public static function start(maps: Array<String>, duration, logger: (String) -> Void) {
        logger('Starting vote for the next map.');
        final mapFiles = Table.toArray(FileLib.Find("maps/*.bsp", GAME).files);
        final validMaps: Array<String> = [];
        for (i in maps) {
            if (mapFiles.contains('${i}.bsp')) {
                validMaps.push(i);
                logger('Adding map to vote: ${i}');
            }
        }
        if (validMaps.length == 0) {
            logger("No valid maps found. Canceling vote.");
            return;
        }
        canVote = true;
        NetLib.Start("mdg.net.mapvote.start");
        NetLib.WriteFloat(Gmod.CurTime() + duration);
        NetLib.WriteInt(validMaps.length, 32);
        for (i in validMaps) {
            NetLib.WriteString(i);
        }
        NetLib.Broadcast();
        TimerLib.Create("fwuso.mdg.timer.vote", duration, 1, () -> {
            canVote = false;
            checkVoteValidity();
            final voteCount = new Map<String, Int>();
            for (i in votes.iterator()) {
                final initial = if (voteCount.exists(i)) voteCount.get(i) else 0;
                voteCount.set(i, initial + 1);
            }
            var mostVoted = {key: "", value: 0};
            for (kv in voteCount.keyValueIterator()) {
                // Choose at random if vote count is equal
                if ((kv.value > mostVoted.value || (kv.value == mostVoted.value && Random.bool())) && validMaps.contains(kv.key)) {
                    mostVoted = kv;
                }
            }
            var chosen = Random.fromArray(validMaps);
            var chosenVotes = 0;
            if (mostVoted.value > 0) {
                chosen = mostVoted.key;
                chosenVotes = mostVoted.value;
            }
            logger('Chosen map: ${chosen} with ${chosenVotes} votes.');

            final changeTime = 10;
            NetLib.Start("mdg.net.mapvote.end");
            NetLib.WriteFloat(Gmod.CurTime() + changeTime);
            NetLib.WriteString(chosen);
            NetLib.Broadcast();
            TimerLib.Create("fwuso.mdg.timer.endvote", changeTime, 1, () -> {
                logger('Changing level to ${chosen}...');
                Gmod.RunConsoleCommand("changelevel", chosen);
            });
        });
    }

    public static function cancel() {
        canVote = false;
        votes.clear();
        TimerLib.Remove("fwuso.mdg.timer.vote");
        TimerLib.Remove("fwuso.mdg.timer.endvote");

        NetLib.Start("mdg.net.mapvote.cancel");
        NetLib.Broadcast();
    }

    static function receiveVote(player: Player, map: String) {
        votes.set(player, map);
        sendVote(player, map);
        checkVoteValidity();
    }

    static function sendVote(player: Player, map: String) {
        NetLib.Start("mdg.net.mapvote.playervote");
        NetLib.WriteEntity(player);
        NetLib.WriteString(map);
        NetLib.Broadcast();
    }

    static function checkVoteValidity() {
        for (player in votes.keys()) {
            if (!Gmod.IsValid(player)) {
                votes.remove(player);
                sendVote(player, "");
            }
        }
    }
    #end
}
