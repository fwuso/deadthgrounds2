package mdg;

import mdg.Utils.LogLevel;
import mdg.Utils.print;
import gmod.libs.HookLib;
import gmod.libs.ConcommandLib;
import gmod.gclass.Vehicle;
import gmod.helpers.TableTools;
import lua.Table;
import gmod.libs.PlayerLib;
import lua.Math;
import gmod.enums.HITGROUP;
import gmod.helpers.types.Angle;
import gmod.libs.NumpadLib;
import gmod.enums.BUTTON_CODE;
import gmod.libs.EntsLib;
import gmod.enums.IN;
import gmod.gclass.Weapon;
import mdg.panels.ScoreBoard;
import gmod.libs.VguiLib;
import gmod.stringtypes.Hook.GMHook;
import mdg.structs.Mission;
import gmod.libs.GameLib;
import gmod.Gmod;
import gmod.libs.TeamLib;
import gmod.libs.UtilLib;
import mdg.MdgState;
import mdg.structs.Hull;
import gmod.libs.NetLib;
import gmod.gclass.CTakeDamageInfo;
import gmod.gclass.Entity;
import gmod.gclass.Vector;
import gmod.gclass.Player;
import gmod.helpers.gamemode.GMBuild;

using mdg.extensions.PlayerExtensions;
using mdg.extensions.EntityExtensions;

// mdg_minplayers -- Minimum player count to start a match
// mdg_lobbytime -- Amount of time to spend in lobby
// mdg_postroundtime -- Amount of time to wait at the end of the match
// mdg_drone_respawntime -- Amount of time it takes for drone players to respawn

inline final MDG_NONE = 1001;
inline final MDG_PLAYERS = 1;
inline final MDG_SPECTATORS = 2;

class GameHooks extends gmod.helpers.gamemode.GMBuild<gmod.gamemode.GM> {
    public final kleiner = Gmod.Model("models/player/kleiner.mdl");

    public var roundsPlayed(get, set): Int;

    function get_roundsPlayed(): Int {
        return Math.round(Gmod.GetGlobalInt("mdg.globals.roundsplayed"));
    }

    function set_roundsPlayed(value: Int): Int {
        #if client
        Gmod.LocalPlayer().ChatPrint("NETWORK VAR SET ON CLIENT; DON'T DO THIS");
        return null;
        #end
        #if server
        Gmod.SetGlobalInt("mdg.globals.roundsplayed", value);
        return value;
        #end
    }

    /*
        final _states = [];
        var gameState(get, set):MdgState;
        function get_gameState():MdgState {
            return _states[Gmod.GetGlobalInt("mdg.globals.gamestate")];
        }

        function set_gameState(value:MdgState):MdgState {
            #if client
            Gmod.LocalPlayer().ChatPrint("NETWORK VAR SET ON CLIENT; DON'T DO THIS");
            return null;
            #end
            #if server
            Gmod.SetGlobalInt("mdg.globals.gamestate", _states.indexOf(value));
            return value;
            #end
        }
     */
    var gameState: MdgState = null;

    final notifier = new Notifier();

    public var defaultState: MdgDefaultState = null;
    public var lobby: MdgLobby = null;
    public var battle: MdgBattle = null;
    public final TEAM_NONE = 1001;
    public final TEAM_PLAYERS = 1;
    public final TEAM_SPECTATORS = 2;
    public var mission: Mission = null;
    public var mapList: Array<String> = [];

    public final playerHull: Hull = {
        mins: new Vector(-17, -17, -1),
        maxs: new Vector(17, 17, 73)
    };

    public final droneHull: Hull = {
        mins: new Vector(-17, -17, -1),
        maxs: new Vector(17, 17, 73)
    };

    #if client
    // var scoreBoard:GScoreBoard = null;
    #end
    public static function preInit() {
        #if server
        Console.createVar("mdg_minplayers", "2", "Minimum amount of players needed to start a round", FCVAR_NOTIFY);
        Console.createVar("mdg_maxrounds", "10", "Rounds to play before letting players vote for a new map", FCVAR_NOTIFY);
        Console.createVar("mdg_zonespeed", "60", "The base rate at which the playable area should shrink, in hammer units / second", FCVAR_NOTIFY);
        Console.createVar("mdg_lobbytime", "15", "Seconds to spend in the lobby before starting each round", FCVAR_NOTIFY);
        Console.createVar("mdg_postroundtime", "10", "Seconds it takes for a new round to start", FCVAR_NOTIFY);
        Console.createVar("mdg_deathtime", "7", "Seconds it takes for a player to spawn as a drone for the first time after death", FCVAR_NOTIFY);
        Console.createVar("mdg_drone_respawntime", "30", "Seconds it takes for a drone to respawn", FCVAR_NOTIFY);
        Console.createVar("mdg_votetime", "30", "Length of the map voting period when max rounds have been reached", FCVAR_NOTIFY);
        ConcommandLib.Add("mdg_restartgame", (ply: Player) -> {
            if (ply.IsAdmin()) {
                HookLib.Run("mdg.event.restartgame");
            }
        }, null, "Restart the game");

        UtilLib.AddNetworkString("mdg.net.clientinit");
        UtilLib.AddNetworkString("mdg.net.dropweapon");
        UtilLib.AddNetworkString("mdg.net.syncstate");
        UtilLib.AddNetworkString("mdg.net.changestate");
        #end
    }

    public function setUp() {
        defaultState = new MdgDefaultState(this);
        lobby = new MdgLobby(this);
        battle = new MdgBattle(this);

        PlayerExtensions.setUp();
        EntityExtensions.setUp();
        MapVoteManager.setUp();
        Theme.setUp();
        Hud.setUp();

        #if server
        HookLib.Add("mdg.event.restartgame", "mdg.hook.restartgame", () -> {
            roundsPlayed = 0;
            getGameState().startState(lobby);
        });

        NetLib.Receive("mdg.net.clientinit", playerJoined);
        NetLib.Receive("mdg.net.dropweapon", (length, player: Player) -> {
            final weapon = player.GetActiveWeapon();
            final direction = player.GetAimVector();
            final pos = player.GetShootPos() + (direction * 8);
            if (Gmod.IsValid(weapon)) {
                final entity: Weapon = EntsLib.Create(weapon.GetClass());
                if (Gmod.IsValid(entity)) {
                    player.StripWeapon(weapon.GetClass());
                    player.ConCommand("lastinv");
                    entity.SetPos(pos);
                    entity.Spawn();
                    entity.SetAngles(player.EyeAngles() + new Angle(0, 90, 0));
                    entity.PhysWake();
                    entity.SetClip1(weapon.Clip1());
                    entity.setPickup(true);
                    final phys = entity.GetPhysicsObject();
                    if (Gmod.IsValid(phys)) {
                        phys.SetVelocity(direction * 400);
                    }
                }
            }
        });
        #end

        #if client
        NetLib.Receive("mdg.net.syncstate", function(length, player) {
            final iter = NetLib.ReadUInt(8);
            for (i in 0...iter) {
                final id = NetLib.ReadUInt(16);
                final wins = NetLib.ReadUInt(16);
                final player = Gmod.Player(id);
                if (Gmod.IsValid(player)) {
                    player.setWinsInitial(wins);
                }
            }
            gameState = intToState(NetLib.ReadUInt(2));
        });

        NetLib.Receive("mdg.net.changestate", function(length, player) {
            gameState.destroy();
            gameState = intToState(NetLib.ReadUInt(2));
            gameState.create();
        });
        #end
    }

    override function GetGameDescription() {
        return "Masa's Deadthgrounds 2";
    }

    override function CreateTeams() {
        TeamLib.SetUp(TEAM_PLAYERS, "Players", Gmod.Color(255, 255, 0));
        TeamLib.SetUp(TEAM_SPECTATORS, "Spectators", Gmod.Color(160, 160, 200));
    }

    override function OnGamemodeLoaded() {
        #if client
        // scoreBoard = ScoreBoard.getInstance();
        #end
    }

    override function Initialize() {
        gameState = defaultState;
        getGameState().create();

        #if server
        final missionManager = new MissionManager("deadthgrounds2/missions/");
        mapList = missionManager.init();
        mission = missionManager.getMapMission(GameLib.GetMap());

        if (mission != null) {
            for (i in [
                {l: mission.lobbyspawns, s: "lobby spawns"},
                // {l: mission.dronespawns, s: "drone spawns"},
                {l: mission.gamespawns, s: "game spawns"}
            ]) {
                if (i.l.length == 0) {
                    mission = null;
                    print('Mission has 0 ${i.s}.', LogLevel.Error);
                }
            }
        }

        if (mission != null) {
            getGameState().startState(lobby);
        } else {
            final castedState: MdgDefaultState = cast getGameState();
            castedState.errorMessage = "Incompatible map. See console for details. Please select another map.";
        }
        #end
    }

    override function Think() {
        getGameState().think();
    }

    override function Tick() {
        getGameState().tick();
    }

    #if client
    override function HUDShouldDraw(name: String): Bool {
        if (name == "CHudAmmo" || name == "CHudSecondaryAmmo" || name == "CHudBattery" || name == "CHudHealth") {
            return false;
        }
        return getGameState().hudShouldDraw(name);
    }

    override function HUDDrawTargetID() {
        getGameState().hudDrawTargetId();
    }

    override function HUDPaintBackground() {
        getGameState().hudPaintBackground();
    }

    override function HUDPaint() {
        Hud.paint();
    }

    override function PostDrawOpaqueRenderables(bDrawingDepth: Bool, bDrawingSkybox: Bool) {
        getGameState().postDrawOpaqueRenderables(bDrawingDepth, bDrawingSkybox);
    }

    override function RenderScreenspaceEffects() {
        getGameState().renderScreenspaceEffects();
    }

    override function PlayerBindPress(ply: Player, bind: String, pressed: Bool): Bool {
        switch bind {
            case "+menu":
                {
                    NetLib.Start("mdg.net.dropweapon");
                    NetLib.SendToServer();
                };
        }
        return false;
    }

    override function ScoreboardShow() {
        final scoreBoard = ScoreBoard.getInstance();
        scoreBoard.Show();
        scoreBoard.MakePopup();
    }

    override function ScoreboardHide() {
        final scoreBoard = ScoreBoard.getInstance();
        scoreBoard.Hide();
    }

    @:gmodHook(GMHook.InitPostEntity)
    static function initPost() {
        NetLib.Start("mdg.net.clientinit");
        NetLib.SendToServer();
    }
    #end

    #if server
    override function PlayerSpawn(player: Player, transition: Bool) {
        player.SetModel(kleiner);
        getGameState().playerSpawn(player, transition);
    }

    override function DoPlayerDeath(player: Player, attacker: Entity, damageInfo: CTakeDamageInfo) {
        if (damageInfo.IsExplosionDamage()) {
            // gib 'em
            // EntsLib.CreateClientProp("");
        }
        player.emitAmbientSound('mdgsnd.player.deadth${Random.int(1, 5)}');
        player.CreateRagdoll();
        getGameState().doPlayerDeath(player, attacker, damageInfo);
    }

    override function PlayerHurt(victim: Player, attacker: Entity, healthRemaining: Float, damageTaken: Float) {}

    static final envCauses = ["trigger_hurt", "worldspawn", "point_hurt"];

    override function PlayerDeath(player: Player, inflictor: Entity, attacker: Entity) {
        if (!Gmod.IsValid(attacker)) {
            attacker = player;
        }

        if (Gmod.IsValid(attacker)) {
            if (envCauses.contains(attacker.GetClass())) {
                attacker = player;
            }
            if (attacker.IsVehicle()) {
                final car: Vehicle = cast attacker;
                if (Gmod.IsValid(car.GetDriver())) {
                    attacker = car.GetDriver();
                }
            }
            if (!Gmod.IsValid(inflictor) && Gmod.IsValid(attacker)) {
                inflictor = attacker;
            }
        }

        if (Gmod.IsValid(inflictor) && inflictor == attacker && (inflictor.IsPlayer() || inflictor.IsNPC())) {
            final killer: Player = cast inflictor;
            inflictor = killer.GetActiveWeapon();
            if (!Gmod.IsValid(inflictor)) {
                inflictor = attacker;
            }
        }

        if (attacker == player) {
            print('${player.Nick()} suicided');
            NetLib.Start("PlayerKilledSelf");
            NetLib.WriteEntity(player);
            NetLib.Broadcast();
        } else if (attacker.IsPlayer()) {
            final killer: Player = cast attacker;
            final wepClass = inflictor.GetClass();
            var wepText = wepClass;
            if (inflictor.IsWeapon()) {
                final wep: Weapon = cast inflictor;
                final wepName = wep.GetPrintName();
                wepText = '${wepName} (${wepClass})';
            }
            print('${player.Nick()} was killed by ${killer.Nick()} with ${wepText}');
            NetLib.Start("PlayerKilledByPlayer");
            NetLib.WriteEntity(player);
            NetLib.WriteString(wepClass);
            NetLib.WriteEntity(killer);
            NetLib.Broadcast();
        } else {
            var killer = Gmod.IsValid(attacker) ? attacker.GetClass() : "nil";
            var inf = Gmod.IsValid(inflictor) ? inflictor.GetClass() : "nil";
            print('${player.Nick()} was killed by ${killer}');
            NetLib.Start("PlayerKilled");
            NetLib.WriteEntity(player);
            NetLib.WriteString(inf);
            NetLib.WriteString(killer);
            NetLib.Broadcast();
        }

        getGameState().playerDeath(player, inflictor, attacker);
    }

    override function PlayerDeathThink(player: Player): Bool {
        return getGameState().playerDeathThink(player);
    }

    override function PlayerDisconnected(player: Player) {
        getGameState().playerDisconnected(player);
    }

    // Disable HEV beeping
    override function PlayerDeathSound(): Bool {
        return true;
    }

    override function CanPlayerSuicide(player: Player): Bool {
        return getGameState().canPlayerSuicide(player);
    }

    override function GetFallDamage(player: Player, speed: Float): Float {
        return (speed - 526.5) * (100 / 396);
    }

    override function PlayerUse(ply: Player, ent: Entity): Bool {
        if (ply.Team() == 2) {
            return false;
        }
        return true;
    }

    override function PlayerCanPickupWeapon(ply: Player, wep: Weapon): Bool {
        if (!wep.isPickup()) {
            return true;
        }
        if (ply.KeyDown(IN.IN_USE)) {
            /*
                final ent = ply.GetEyeTrace().Entity;
                if (Gmod.IsValid(ent)) {
                    if (ent == wep) {
             */
            return true;
            /*
                    }
                }
             */
        }
        return false;
    }

    override function PlayerButtonDown(ply: Player, button: BUTTON_CODE) {
        NumpadLib.Activate(ply, cast button, false);
    }

    override function PlayerButtonUp(ply: Player, button: BUTTON_CODE) {
        NumpadLib.Deactivate(ply, cast button, false);
    }

    override function ScalePlayerDamage(ply: Player, hitgroup: HITGROUP, dmginfo: CTakeDamageInfo): Bool {
        final scale = switch hitgroup {
            case HITGROUP_HEAD: {
                    if (ply.playerData().hasHelmet) {
                        ply.setHelmet(false);
                        ply.EmitSound("mdgsnd.hit.helmet");
                        0.5;
                    } else {
                        ply.setHelmet(false);
                        2;
                    }
                };
            case HITGROUP_LEFTARM, HITGROUP_RIGHTARM, HITGROUP_LEFTLEG, HITGROUP_RIGHTLEG, HITGROUP_GEAR: 0.25;
            default: return false;
        }
        dmginfo.ScaleDamage(scale);
        return false;
    }

    function OnEntityWaterLevelChanged(entity: Entity, old: Float, now: Float) {
        final wl = Math.round(now);
        if (entity.IsPlayer()) {}
    }

    function playerJoined(_: Int, player: Player) {
        final players = PlayerLib.GetAll();
        NetLib.Start("mdg.net.syncstate");
        NetLib.WriteUInt(TableTools.length(players), 8);
        for (i in players) {
            final id = i.UserID();
            final wins = i.getWins();
            NetLib.WriteUInt(id, 16);
            NetLib.WriteUInt(wins, 16);
        }
        final value = stateToInt(gameState);
        NetLib.WriteUInt(value, 2);
        NetLib.Send(player);
        getGameState().playerJoined(player);
    }

    public function notifyAll(message: String, ?minDisplayTime: Null<Float>, ?maxDisplayTime: Null<Float>) {
        notifier.notifyAll(message, minDisplayTime, maxDisplayTime);
    }

    public function announceWinner(player: Player) {
        notifier.announceWinner(player);
    }

    public function setGameState(state: MdgState) {
        notifier.reset();
        MapVoteManager.cancel();

        gameState = state;
        NetLib.Start("mdg.net.changestate");
        final value = stateToInt(state);
        NetLib.WriteUInt(value, 2);
        NetLib.Broadcast();
    }
    #end

    public inline function getGameState() {
        return gameState;
    }

    function stateToInt(state: MdgState): Int {
        return switch state {
            case v if (Std.isOfType(v, MdgLobby)): 1;
            case v if (Std.isOfType(v, MdgBattle)): 2;
            case v if (Std.isOfType(v, MdgDefaultState)): 3;
            default: 0;
        }
    }

    function intToState(num: Int): MdgState {
        return switch num {
            case 1: lobby;
            case 2: battle;
            case 3: defaultState;
            default: null;
        }
    }
}
