package mdg.extensions;

class Units {
    static final huInM = 42;

    public static function mToHammerUnits(arg: Float): Float {
        return arg * huInM;
    }

    public static function hammerUnitsToMeters(arg: Float): Float {
        return arg / huInM;
    }
}
