package mdg.extensions;

import gmod.enums.CHAN;
import gmod.enums.SNDLVL;
import gmod.libs.UtilLib;
import gmod.libs.NetLib;
import gmod.gclass.Entity;
import mdg.panels.ScoreBoardPlayer;
import gmod.gclass.Player;
import gmod.gclass.Vector;
import Math;
import Std;

class EntityExtensions {
    static final netEmitAmbientSound = "mdg.net.emitambientsound";

    public static function setUp() {
        #if server
        UtilLib.AddNetworkString(netEmitAmbientSound);
        #end

        #if client
        NetLib.Receive(netEmitAmbientSound, () -> {
            final entity = NetLib.ReadEntity();
            final sound = NetLib.ReadString();
            final level = NetLib.ReadUInt(8);
            final pitchPercent = NetLib.ReadFloat();
            final volume = NetLib.ReadFloat();
            final channel = NetLib.ReadUInt(8);
            entity.EmitSound(sound, level, pitchPercent, volume, cast(channel, CHAN));
        });
        #end
    }

    #if server
    static final dataMap = new Map<Entity, EntData>();

    public static function setPickup(entity: Entity, pickup: Bool) {
        entData(entity).pickup = pickup;
    }

    public static function isPickup(entity: Entity): Bool {
        return entData(entity).pickup;
    }

    /**
     * Emit sound on client side for each player, bypassing the PAS
     * @param entity
     */
    public static function emitAmbientSound(entity: Entity, sound: String, level: Int = 75, pitchPercent: Float = 100, volume: Float = 1.0, channel: Int = 0) {
        NetLib.Start(netEmitAmbientSound);
        NetLib.WriteEntity(entity);
        NetLib.WriteString(sound);
        NetLib.WriteUInt(level, 8);
        NetLib.WriteFloat(pitchPercent);
        NetLib.WriteFloat(volume);
        NetLib.WriteUInt(channel, 8);
        NetLib.Broadcast();
    }

    static function entData(ent: Entity): EntData {
        final data = dataMap.get(ent);
        if (data != null) {
            return data;
        }
        final newData = {
            pickup: false
        };
        dataMap.set(ent, newData);
        return newData;
    }
    #end
}

#if server
typedef EntData = {
    pickup: Bool,
}
#end
