package mdg.extensions;

import gmod.Gmod;
import gmod.libs.Player_managerLib;
import gmod.libs.NetLib;
import gmod.libs.UtilLib;
import mdg.panels.ScoreBoardPlayer;
import gmod.gclass.Player;
import gmod.gclass.Vector;
import Math;
import Std;

class PlayerExtensions {
    static final modelList = ["01", "02", "03", "04", "05", "06", "07", "09", "11"];
    static final dataMap = new Map<Player, PlayerData>();
    static final playerWins = new Map<Player, Int>();

    #if client
    public static function setWinsInitial(player: Player, num: Int) {
        playerWins.set(player, num);
    }
    #end

    public static function setUp() {
        for (i in modelList) {
            Player_managerLib.AddValidModel('conscript${i}', 'models/jessev92/hl2/conscripts/m${i}_ply.mdl');
            Player_managerLib.AddValidHands('conscript${i}', 'models/weapons/c_arms_cstrike.mdl', 0, '0');
        }

        #if server
        UtilLib.AddNetworkString("mdg.net.wins");
        UtilLib.AddNetworkString("mdg.net.hashelmet");
        #end

        #if client
        NetLib.Receive("mdg.net.wins", function(length, nullValue) {
            final pl: Player = cast NetLib.ReadEntity();
            final wins = NetLib.ReadInt(16);
            playerWins.set(pl, wins);
        });
        NetLib.Receive("mdg.net.hashelmet", function(length, nullValue) {
            final hasIt = NetLib.ReadBool();
            playerData(Gmod.LocalPlayer()).hasHelmet = hasIt;
        });
        #end
    }

    #if server
    public static function init(player: Player, position: Vector) {
        final modelInex = modelList[Std.random(modelList.length)];
        setHelmet(player, false);
        player.GodDisable();
        player.StripWeapons();
        player.StripAmmo();
        player.SetModel('models/jessev92/hl2/conscripts/m${modelInex}_ply.mdl');
        player.UnSpectate();
        player.SetupHands(null);
        player.AllowFlashlight(true);
        player.SetCanZoom(false);
        player.SetWalkSpeed(130);
        player.SetRunSpeed(250);
        player.SetCrouchedWalkSpeed(0.65);
        player.SetNoCollideWithTeammates(false);
        restore(player);
        // Skin - random
        player.SetSkin(Std.random(Math.floor(player.SkinCount())));
        // Vest - none
        player.SetBodygroup(1, 0);
        // Hands - random
        final handGroup = Std.random(3);
        player.SetBodygroup(2, handGroup);
        player.SetBodygroup(3, handGroup);
        // Headgear - none
        player.SetBodygroup(4, 7);
        player.SetPos(position);
    }

    public static function restore(player: Player) {
        player.SetMaxHealth(100);
        player.SetHealth(100);
        player.SetArmor(0);
    }

    public static function addWins(player: Player, num: Int) {
        final existing = if (playerWins.exists(player)) playerWins.get(player) else 0;
        final wins = num + existing;
        playerWins.set(player, wins);
        NetLib.Start("mdg.net.wins");
        NetLib.WriteEntity(player);
        NetLib.WriteInt(wins, 16);
        NetLib.Broadcast();
    }

    public static function setHelmet(player: Player, give: Bool) {
        if (give) {
            player.SetBodygroup(4, Random.int(0, 3));
        } else {
            player.SetBodygroup(4, 7);
        }
        playerData(player).hasHelmet = give;
        NetLib.Start("mdg.net.hashelmet");
        NetLib.WriteBool(give);
        NetLib.Send(player);
    }

    public static function setBodyArmor(player: Player, amount: Int) {
        if (amount > 0) {
            player.SetArmor(Math.max(amount, 100));
            player.SetBodygroup(1, 4);
        } else {
            player.SetArmor(0);
            player.SetBodygroup(1, 0);
        }
    }

    public static function randomHat(player: Player) {
        if (player.GetBodygroup(4) == 7.0) {
            player.SetBodygroup(4, Random.int(4, 6));
        }
    }
    #end

    public static function getWins(player: Player): Int {
        final hasWins = playerWins.exists(player);
        return if (hasWins) playerWins.get(player) else 0;
    }

    public static function playerData(player: Player): PlayerData {
        final data = dataMap.get(player);
        if (data != null) {
            return data;
        }
        final newData = {
            #if server
            nextSpawnTime: 0.0,
            #end
            hasHelmet: false,
            #if client
            scoreBoardEntry: null,
            #end
        };
        dataMap.set(player, newData);
        return newData;
    }

    public static function showHint(player: Player, message: String) {
        // print 5 msgs at most
        var msgs = [];
        for (i in 0...5) {
            var substr = message.substring(i * 255, (i + 1) * 255);
            if (substr.length == 0) {
                break;
            }
            msgs.push(substr);
        }
        for (m in msgs) {
            player.ChatPrint(m);
        }
    }
}

typedef PlayerData = {
    #if server
    nextSpawnTime: Float,
    #end
    hasHelmet: Bool,
    #if client
    scoreBoardEntry: Null<ScoreBoardPlayer>,
    #end
}
