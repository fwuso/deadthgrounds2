package mdg;

import gmod.libs.SurfaceLib;
import gmod.Gmod;

class Theme {
    public static function setUp() {
        #if client
        SurfaceLib.CreateFont("Mdg2TextTiny", {
            font: "JetBrains Mono NL",
            size: 16
        });
        SurfaceLib.CreateFont("Mdg2TextSmall", {
            font: "JetBrains Mono NL",
            size: 20
        });
        SurfaceLib.CreateFont("Mdg2TitleSmall", {
            font: "Bungee",
            size: 34
        });
        SurfaceLib.CreateFont("Mdg2TextLarge", {
            font: "JetBrains Mono NL",
            size: 36
        });
        SurfaceLib.CreateFont("Mdg2TitleLarge", {
            font: "Bungee",
            size: 64
        });
        #end
    }

    inline public static final textFontTiny = "Mdg2TextTiny";
    inline public static final textFontSmall = "Mdg2TextSmall";
    inline public static final titleFontSmall = "Mdg2TitleSmall";
    inline public static final textFontLarge = "Mdg2TextLarge";
    inline public static final titleFontLarge = "Mdg2TitleLarge";

    inline public static function rgbPercentage(fl: Float): Float {
        return fl / 255;
    }

    public static final sphereColor = Gmod.Color(178, 25, 25);
    public static final textColor = Gmod.Color(255, 255, 255);
    public static final textShadow = Gmod.Color(32, 32, 32, 40);
    public static final notificationColor = Gmod.Color(25, 25, 30, 200);
    public static final notificationTextColor = Gmod.Color(255, 255, 255);
    public static final dangerColor = Gmod.Color(178, 25, 25);

    inline public static final paddingWidth = 64;
    inline public static final paddingHeight = 16;
    inline public static final cornerRadius = 4;

    inline public static final scoreBoardMargin = 8;
    inline public static final scoreBoardPadding = 4;
}
