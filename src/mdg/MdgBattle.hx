package mdg;

import mdg.ents.Mdg_Artillery_Strike;
import mdg.Utils.print;
import gmod.helpers.LuaArray;
import mdg.Utils.traceDir;
import mdg.GameHooks.MDG_SPECTATORS;
import mdg.Utils.traceSky;
import gmod.libs.SoundLib;
import gmod.structs.Trace;
import gmod.libs.UtilLib;
import mdg.ents.Mdg_Vest;
import gmod.gclass.Weapon;
import mdg.structs.Mission;
import gmod.helpers.types.Angle;
import gmod.libs.MathLib;
import mdg.Utils.spawnSimfphys;
import gmod.helpers.TableTools;
import gmod.libs.EngineLib;
import gmod.gclass.IMaterial;
import gmod.libs.RenderLib;
import gmod.helpers.net.NET_Server;
import gmod.gclass.Color;
import gmod.libs.DrawLib;
import gmod.libs.SurfaceLib;
import gmod.libs.CamLib;
import gmod.libs.TimerLib;
import gmod.libs.EntsLib;
import gmod.Gmod;
import gmod.libs.TeamLib;
import gmod.libs.GameLib;
import lua.Table;
import gmod.libs.PlayerLib;
import gmod.gclass.CTakeDamageInfo;
import gmod.gclass.Entity;
import gmod.gclass.Player;
import gmod.gclass.Vector;

using mdg.extensions.PlayerExtensions;
using mdg.extensions.EntityExtensions;
using mdg.extensions.Units;

class MdgBattle extends MdgState {
    #if client
    var sphereMaterial: IMaterial = Gmod.Material("mdg/sphere.vmt").a;
    final finalePosIcon = Gmod.Material("materials/mdg/hud/finale.png").a;
    final colorModify = Table.fromMap([
        "$pp_colour_addr" => Theme.rgbPercentage(Theme.sphereColor.r) * 0.35,
        "$pp_colour_addg" => Theme.rgbPercentage(Theme.sphereColor.g) * 0.35,
        "$pp_colour_addb" => Theme.rgbPercentage(Theme.sphereColor.b) * 0.35,
        "$pp_colour_brightness" => -0.05,
        "$pp_colour_contrast" => 1,
        "$pp_colour_colour" => 1,
        "$pp_colour_mulr" => Theme.rgbPercentage(Theme.sphereColor.r),
        "$pp_colour_mulg" => Theme.rgbPercentage(Theme.sphereColor.g),
        "$pp_colour_mulb" => Theme.rgbPercentage(Theme.sphereColor.b)
    ]);
    #end

    #if server
    var gameSpawns: SpawnManager = null;
    var spectatorSpawns: SpawnManager = null;

    final events: Array<(Mission) -> String> = [];

    var startedSiren = false;
    var droppedBomb = false;
    #end

    final originalSphereRadius = 45000.0;
    var sphereRadius(get, set): Float;
    var finalePos(get, set): Vector;
    var enableFinalePos(get, set): Bool;

    function get_enableFinalePos(): Bool {
        return Gmod.GetGlobalBool("mdg.globals.enablefpos", false);
    }

    function set_enableFinalePos(value: Bool): Bool {
        #if client
        Gmod.LocalPlayer().ChatPrint("NETWORK VAR SET ON CLIENT; DON'T DO THIS");
        return false;
        #end
        #if server
        Gmod.SetGlobalBool("mdg.globals.enablefpos", value);
        return value;
        #end
    }

    function get_finalePos(): Vector {
        return Gmod.GetGlobalVector("mdg.globals.finalepos", new Vector(0, 0, 0));
    }

    function set_finalePos(value: Vector): Vector {
        #if client
        Gmod.LocalPlayer().ChatPrint("NETWORK VAR SET ON CLIENT; DON'T DO THIS");
        return new Vector(0, 0, 0);
        #end
        #if server
        Gmod.SetGlobalVector("mdg.globals.finalepos", value);
        return value;
        #end
    }

    function get_sphereRadius(): Float {
        return Gmod.GetGlobalFloat("mdg.globals.sphereradius", 0.0);
    }

    function set_sphereRadius(value: Float): Float {
        #if client
        Gmod.LocalPlayer().ChatPrint("NETWORK VAR SET ON CLIENT; DON'T DO THIS");
        return 0.0;
        #end
        #if server
        Gmod.SetGlobalFloat("mdg.globals.sphereradius", value);
        return value;
        #end
    }

    var tickInterval = EngineLib.TickInterval();
    var shrinkRate = -1;
    var postRoundTime = -1;
    var droneRespawnTime = -1;

    public function new(manager: GameHooks) {
        #if server
        events.push(artilleryEvent);
        events.push(supplyDropEvent);
        events.push(nuclearEvent);
        #end

        super(manager);
    }

    static final starterWeps = ["doi_atow_etoolus", "doi_atow_etoolde", "doi_atow_etoolcw"];

    override function create() {
        tickInterval = EngineLib.TickInterval();
        // TODO: Replace with some sort of lazy eval
        #if server
        droppedBomb = false;
        startedSiren = false;

        manager.roundsPlayed += 1;
        shrinkRate = Console.getVarInt("mdg_zonespeed");
        postRoundTime = Console.getVarInt("mdg_postroundtime");
        droneRespawnTime = Console.getVarInt("mdg_drone_respawntime");
        if (gameSpawns == null) {
            gameSpawns = new SpawnManager(manager.mission.gamespawns.map(p -> new Vector(p.pos[0], p.pos[1], p.pos[2])), manager.playerHull);
        }
        if (spectatorSpawns == null) {
            spectatorSpawns = new SpawnManager(manager.mission.dronespawns.map(p -> new Vector(p.pos[0], p.pos[1], p.pos[2])), manager.droneHull);
        }
        for (player in PlayerLib.GetAll()) {
            player.SetTeam(manager.TEAM_PLAYERS);
            if (player.Alive()) {
                player.restore();
            } else {
                player.Spawn();
            }
            player.SetPos(gameSpawns.next());
            player.Give(Random.fromArray(starterWeps));
        }
        final fpoint = getFinalePoint();
        sphereRadius = originalSphereRadius;
        finalePos = fpoint;
        enableFinalePos = true;

        TimerLib.Create("fwuso.mdg.timer.spheredamage", 3, 0, () -> {
            final array: Array<Player> = Table.toArray(cast TeamLib.GetPlayers(manager.TEAM_PLAYERS));
            for (player in array) {
                final dist = player.GetPos().Distance(fpoint);
                if (dist > sphereRadius) {
                    player.TakeDamage(4, player, player);
                }
            }
        });
        TimerLib.Create("fwuso.mdg.timer.event", 400, 1, () -> {
            final event = Random.fromArray(events);
            final message = event(manager.mission);
            if (message.length == 0) {
                return;
            }
            manager.notifyAll(message, 5);
        });
        spawnMissionEntities();
        manager.notifyAll("Good Huntign,,,", 5);
        #end
    }

    override function tick() {
        #if server
        var speedMultiplier = 1.0;
        if (sphereRadius < 3000) {
            speedMultiplier = Math.sin(((1.57 / 3000) * Math.max(sphereRadius, 1500)) - 1.57) + 1;
        }

        if (sphereRadius > 64) {
            sphereRadius -= shrinkRate * tickInterval * speedMultiplier;
        } else if (!droppedBomb) {
            droppedBomb = true;
            var bomb: Entity = EntsLib.Create("hb_main_implosionbomb_edit");
            if (Gmod.IsValid(bomb)) {
                bomb.SetPos(finalePos);
                bomb.Spawn();
                untyped __lua__('bomb:Arm();bomb:ExplodeEDIT()');
            }
            Gmod.BroadcastLua('RunConsoleCommand("stopsound")');
        }

        if (!startedSiren && sphereRadius < 800) {
            startedSiren = true;
            Gmod.BroadcastLua('surface.PlaySound("gbombs_5/sirens/nuclear_siren.wav")');
        }
        #end
    }

    override function destroy() {
        #if client
        Gmod.RunConsoleCommand("stopsound");
        #end

        #if server
        enableFinalePos = false;
        TimerLib.Remove("fwuso.mdg.timer.event");
        TimerLib.Remove("fwuso.mdg.timer.freezeitems");
        TimerLib.Remove("fwuso.mdg.timer.spheredamage");
        TimerLib.Remove("fwuso.mdg.timer.postround");
        #end
    }

    #if server
    function supplyDropEvent(mission: Mission): String {
        var supplySpawns = mission.supplies.copy();

        final amount = Random.int(3, Std.int(Math.max(4, mission.supplies.length - 1)));
        for (_ in 0...amount) {
            if (supplySpawns.length == 0) {
                supplySpawns = mission.supplies.copy();
            }
            final point = Random.fromArray(supplySpawns);
            supplySpawns.remove(point);
            final startPos = new Vector(Random.int(-128, 128) + point.pos[0], Random.int(-128, 128) + point.pos[1], point.pos[2]);
            final spawnPos = traceSky(startPos);
            if (spawnPos == null) {
                continue;
            }
            var ground = traceDir(startPos, new Vector(0, 0, -1)).HitPos.z;
            startPos.z = Math.min(startPos.z, ground + 6000);
            spawnPos.z -= Random.float(80, 320);
            final drop: Entity = EntsLib.Create("mdg_supplydrop");
            if (Gmod.IsValid(drop)) {
                drop.SetPos(spawnPos);
                drop.Spawn();
            }
        }

        return '${amount} Supply Drops incoming';
    }

    function nuclearEvent(mission: Mission): String {
        var vec = Random.fromArray(mission.finales).pos;
        var nukeSpawn = traceSky(new Vector(vec[0], vec[1], vec[2]));
        if (nukeSpawn == null) {
            return "";
        }
        nukeSpawn.z -= 80;
        var nuke: Entity = EntsLib.Create("hb_nuclear_littleboy");
        if (Gmod.IsValid(nuke)) {
            nuke.SetPos(nukeSpawn);
            nuke.Spawn();
            untyped __lua__('nuke:Arm()');
        }
        return "Nuclear Airstrike Inbound";
    }

    function artilleryEvent(mission: Mission): String {
        final finales = mission.finales.map((p) -> new Vector(p.pos[0], p.pos[1], p.pos[2]));
        final betweenPlyAndFinale = [for (ply in PlayerLib.GetAll()) ply].filter((p) -> Gmod.IsValid(p) && p.Team() == manager.TEAM_PLAYERS).map((p) -> {
            final pPos = p.GetPos();
            final diff = (finalePos - pPos) / 2;
            return pPos + diff;
        });
        final candidates = finales.concat(betweenPlyAndFinale);
        Random.shuffle(candidates);
        var pos: Vector = null;
        for (c in candidates) {
            if (traceSky(c) != null) {
                pos = c;
                break;
            }
        }
        if (pos == null) {
            return "";
        }
        var strike = EntsLib.Create("mdg_artillery_strike");
        if (Gmod.IsValid(strike)) {
            strike.SetPos(pos);
            strike.Spawn();
        }
        return "Artillery Strike inbound";
    }

    function checkBattleDeath(killed: Player, reason: String) {
        final aliveCount = TeamLib.NumPlayers(manager.TEAM_PLAYERS);
        if (aliveCount == 0) {
            return;
        }
        final players: Array<Player> = Table.toArray(cast TeamLib.GetPlayers(manager.TEAM_PLAYERS));
        manager.notifyAll('${killed.Nick()} ${reason}!', 2);
        trace('alive: ${aliveCount}');
        if (aliveCount == 1) {
            final winner = players[0];
            winner.addWins(1);
            manager.announceWinner(winner);
            TimerLib.Create("fwuso.mdg.timer.postround", postRoundTime, 1, function() {
                if (manager.roundsPlayed >= Console.getVarInt("mdg_maxrounds")) {
                    startState(manager.defaultState);
                    MapVoteManager.start(manager.mapList, Console.getVarInt("mdg_votetime"), print.bind(_));
                } else {
                    startState(manager.lobby);
                }
            });
        } else if (aliveCount == 2) {
            final candidate1 = players[0].Nick();
            final candidate2 = players[1].Nick();
            manager.notifyAll('${candidate1} vs ${candidate2}', 2);
        } else {
            manager.notifyAll('${aliveCount} combatants remaining');
        }
    }

    function spawnMissionEntities() {
        final mission = manager.mission;
        final mediumVehicles = [
            "sim_fphys_pwhatchback",
            "sim_fphys_pwvan",
            "sim_fphys_van",
            "sim_fphys_pwmoskvich",
            "sim_fphys_pwtrabant",
            "sim_fphys_v8elite"
        ];
        final largeVehicles = [
            "sim_fphys_pwliaz",
            "sim_fphys_pwavia",
            "sim_fphys_conscriptapc_armed",
            "sim_fphys_v8elite_armed2"
        ];
        final helicopters = ["wac_hc_littlebird_ah6", "wac_hc_littlebird_mh6", "wac_hc_ah1z_viper"];
        final itemSchema = [
            {
                name: "Small Weapons",
                percentage: 0.16,
                collections: [
                    {
                        name: "modern",
                        items: [
                            "cw_makarov",
                            "cw_p99",
                            "cw_mr96",
                            "doi_atow_m1911a1",
                            "cw_deagle",
                            "cw_fiveseven",
                        ]
                    },
                    {
                        name: "ww2",
                        items: [
                            "doi_atow_p08",
                            "doi_atow_p38",
                            "doi_atow_ppk",
                            "doi_atow_sw1917",
                            "doi_atow_m1911a1",
                            "doi_atow_browninghp",
                            "doi_atow_bren"
                        ]
                    }
                ]
            },
            {
                name: "Medium Weapons",
                percentage: 0.21,
                collections: [
                    {
                        name: "modern",
                        items: [
                            "cw_ak74", "cw_ar15", "cw_famasg2_official", "cw_scarh", "cw_g3a3", "cw_g36c", "cw_l85a2", "cw_m14", "cw_xm1014_official",
                            "cw_saiga12k_official", "cw_svd_official", "cw_vss", "cw_shorty", "cw_mp9_official", "cw_mac11", "cw_m3super90", "cw_ump45",
                            "cw_mp5",
                            "cw_mp7_official", "doi_atow_enfield", "doi_ws_atow_kp31"
                        ]
                    },
                    {
                        name: "ww2",
                        items: [
                            "doi_atow_c96", "doi_atow_c96carbine", "doi_atow_fg42", "doi_atow_g43", "doi_atow_k98k", "doi_ws_atow_kp31", "doi_atow_enfield",
                            "doi_atow_lewis", "doi_atow_m1carbine", "doi_atow_m1garand", "doi_atow_m1903a3", "doi_atow_m1912", "doi_atow_m1918a2",
                            "doi_atow_m1928a1",
                            "doi_atow_m1a1", "doi_atow_m1a1carbine", "doi_atow_m3greasegun", "doi_atow_ithaca37", "doi_ws_atow_mp34", "doi_atow_mp40",
                            "doi_atow_owen", "doi_atow_sten", "doi_atow_stg44", "doi_atow_webley", "doi_atow_welrod"
                        ]
                    }
                ]
            },
            {
                name: "Heavy Weapons",
                percentage: 0.09,
                collections: [
                    {name: "modern", items: ["cw_l115", "cw_m249_official", "ins2_atow_rpg7"]},
                    {
                        name: "ww2",
                        items: ["doi_atow_vickers", "doi_atow_mg34", "doi_atow_mg42", "doi_atow_m1919a6"]
                    }
                ]
            },
            {
                name: "Ammo",
                percentage: 0.15,
                collections: [
                    {
                        name: "modern",
                        items: [
                            "cw_ammo_kit_small",
                            "cw_ammo_kit_regular",
                            "cw_ammo_crate_small",
                            "cw_ammo_fraggrenades"
                        ]
                    }
                ]
            },
            {
                name: "Misc Items",
                percentage: 0.18,
                collections: [
                    {
                        name: "modern",
                        items: [
                            "mdg_lootcrate",
                            "seal6-claymore",
                            "cw_smoke_grenade",
                            "cw_frag_grenade",
                            "cw_flash_grenade"
                        ]
                    }
                ]
            },
            {
                name: "Medical Items",
                percentage: 0.07,
                collections: [{name: "modern", items: ["sg_medkit_fix", "sg_adrenaline_fix"]}]
            },
            {name: "Armor", percentage: 0.14, collections: [{name: "modern", items: ["mdg_vest", "mdg_helmet", "mdg_vesthelmet"]}]}
        ];

        // Remove possible HL2 items
        final hlItems = [
            "weapon_pistol",
            "weapon_shotgun",
            "weapon_smg1",
            "item_ammo_crate",
            "item_ammo_pistol",
            "item_ammo_smg1",
            "item_box_buckshot",
            "item_healthkit",
            "item_item_crate"
        ];
        for (className in hlItems) {
            var instances: LuaArray<Entity> = EntsLib.FindByClass(className);
            for (item in instances) {
                item.Remove();
            }
        }

        final itemSpawns = mission.items.copy();

        final itemCount = MathLib.Clamp(itemSpawns.length * 0.66, 0, 100);
        final freezables: Array<Entity> = [];

        // Spawn items
        for (category in itemSchema) {
            final amount = Math.ceil(category.percentage * itemCount);
            print('${category.name} - inserting ${amount} items');
            for (_ in 0...amount) {
                final item = Random.fromArray(category.collections[0].items);
                final place = Random.fromArray(itemSpawns);

                final entity = spawnItem(item, place);
                if (entity != null) {
                    itemSpawns.remove(place);
                    freezables.push(entity);
                }
            }
        }

        final vehicleCount = Math.round(MathLib.Clamp(mission.vehicles.medium.length / 2, 0, 7));
        final vehicleSpawns = mission.vehicles.medium.copy();

        for (_ in 0...vehicleCount) {
            final place = Random.fromArray(vehicleSpawns);
            spawnSimfphys(Random.fromArray(mediumVehicles), new Vector(place.pos[0], place.pos[1], place.pos[2]),
                new Angle(place.ang[0], place.ang[1], place.ang[2]));
            vehicleSpawns.remove(place);
        }

        final largeVehicleCount = Math.round(MathLib.Clamp((mission.vehicles.large.length + mission.helicopters.length) / 2, 0, 2));
        final heliSpawns = mission.helicopters.copy();
        final largeVehicleSpawns = mission.vehicles.large.copy();
        final hasHelis = heliSpawns.length > 0;
        final hasBigRigs = largeVehicleSpawns.length > 0;

        for (_ in 0...largeVehicleCount) {
            if (Random.bool() && hasHelis) {
                final place = Random.fromArray(heliSpawns);
                final heli = Random.fromArray(helicopters);
                final entity: Entity = EntsLib.Create(heli);
                if (Gmod.IsValid(entity)) {
                    entity.SetPos(new Vector(place.pos[0], place.pos[1], place.pos[2]));
                    entity.Spawn();
                    entity.SetSkin(Std.random(Math.round(entity.SkinCount())));
                    heliSpawns.remove(place);
                }
            } else if (hasBigRigs) {
                final place = Random.fromArray(largeVehicleSpawns);
                spawnSimfphys(Random.fromArray(largeVehicles), new Vector(place.pos[0], place.pos[1], place.pos[2] + 32),
                    new Angle(place.ang[0], place.ang[1] - 90, place.ang[2]));
                largeVehicleSpawns.remove(place);
            }
        }

        TimerLib.Create("fwuso.mdg.timer.freezeitems", 5, 1, () -> {
            for (item in freezables) {
                if (Gmod.IsValid(item)) {
                    final phys = item.GetPhysicsObject();
                    if (Gmod.IsValid(phys)) {
                        phys.EnableMotion(false);
                    }
                }
            }
        });
        // gm_fork: 200 item spawns
        // TODO: Make most entities sleep ~5 seconds after spawning
    }

    function spawnItem(className: String, place: PosAng): Null<Entity> {
        final entity: Entity = EntsLib.Create(className);
        if (Gmod.IsValid(entity)) {
            entity.SetPos(new Vector(place.pos[0], place.pos[1], place.pos[2]));
            entity.SetAngles(new Angle(place.ang[0], place.ang[1] + 90, place.ang[2]));
            entity.Spawn();
            entity.setPickup(true);
            return entity;
        }
        return null;
    }

    function getFinalePoint(): Vector {
        final finale = Random.fromArray(manager.mission.finales);
        return new Vector(finale.pos[0], finale.pos[1], finale.pos[2]);
    }

    function createDrone(player: Player) {
        /*
            final ent:Entity = EntsLib.Create("prop_physics");
            ent.SetModel("models/hunter/misc/sphere025x025.mdl");
            ent.SetPos(player.GetPos());
            ent.Spawn();

            player.Spectate(OBS_MODE_CHASE);
            player.SpectateEntity(ent);
         */

        player.Spectate(OBS_MODE_ROAMING);
        player.GodEnable();
        player.AllowFlashlight(false);
    }

    function droneDestroyed(player: Player) {
        final time = Gmod.CurTime();
        player.KillSilent();
        player.playerData().nextSpawnTime = time + droneRespawnTime;
    }
    #end

    // Hooks
    #if client
    override function hudShouldDraw(name: String): Bool {
        if (Gmod.LocalPlayer().Team() == MDG_SPECTATORS
            && (name == "CHudAmmo" || name == "CHudSecondaryAmmo" || name == "CHudBattery" || name == "CHudHealth" || name == "CHudCrosshair")) {
            return false;
        }
        return true;
    }

    override function hudPaintBackground() {
        final fpos = finalePos;
        if (enableFinalePos) {
            final playerPos = Gmod.LocalPlayer().GetPos();
            final dist = fpos.Distance(playerPos);
            final text = 'Finale - ${Math.round(dist.hammerUnitsToMeters())}m';
            final textPos = fpos.ToScreen();
            final iconSize = 16;

            SurfaceLib.SetDrawColor(Theme.dangerColor);
            SurfaceLib.SetMaterial(finalePosIcon);
            SurfaceLib.DrawTexturedRect(textPos.x - (iconSize / 2), textPos.y - (iconSize / 2), iconSize, iconSize);

            DrawLib.SimpleTextOutlined(text, Theme.textFontTiny, textPos.x, textPos.y + 24, Gmod.Color(255, 255, 255), TEXT_ALIGN_CENTER, null, 2,
                Theme.textShadow);
        }
    }

    override function postDrawOpaqueRenderables(bDrawingDepth: Bool, bDrawingSkybox: Bool) {
        if (bDrawingSkybox) {
            return;
        }
        final fpos = finalePos;
        final dist = fpos.Distance(Gmod.LocalPlayer().GetPos());
        final diff = sphereRadius - dist;
        final alphaMultiplier = 1 - MathLib.Remap(MathLib.Clamp(diff, 1024, 1536), 1024, 1536, 0, 1);
        final alpha = Gmod.Lerp(alphaMultiplier, 0, 127);
        final red = Theme.sphereColor.r;
        final green = Theme.sphereColor.g;
        final blue = Theme.sphereColor.b;
        if (enableFinalePos) {
            RenderLib.SetMaterial(sphereMaterial);
            RenderLib.DrawSphere(fpos, sphereRadius, 32, 32, Gmod.Color(red, green, blue, alpha));
        }
    }

    override function renderScreenspaceEffects() {
        final fpos = finalePos;
        // TODO: Remove the hack
        final player = Gmod.LocalPlayer();
        var huutis = false;
        final a: Array<Player> = Table.toArray(cast TeamLib.GetPlayers(manager.TEAM_PLAYERS));
        for (i in a) {
            if (i.UserID() == player.UserID()) {
                huutis = true;
            }
        }
        if (enableFinalePos && huutis) {
            final dist = fpos.Distance(player.GetPos());
            if (dist > sphereRadius) {
                Gmod.DrawColorModify(colorModify);
            }
        }
    }
    #end

    #if server
    override function playerSpawn(player: Player, transition: Bool) {
        player.init(gameSpawns.next());
        if (player.Team() != manager.TEAM_PLAYERS) {
            player.SetTeam(manager.TEAM_SPECTATORS);
            // player.init(spectatorSpawns.next());
            createDrone(player);
        }
    }

    override function doPlayerDeath(player: Player, attacker: Entity, damageInfo: CTakeDamageInfo) {
        player.AddDeaths(1);
        if (attacker.IsValid() && player.IsValid() && attacker.IsPlayer()) {
            if (player != attacker) {
                (cast attacker).AddFrags(1);
            }
        }
        final position = player.GetPos() + new Vector(0, 0, 36);
        for (weapon in player.GetWeapons()) {
            final entity: Weapon = cast deathDropItem(weapon.GetClass(), position);
            if (entity != null) {
                entity.SetClip1(weapon.Clip1());
                entity.setPickup(true);
            }
        }
        if (player.Armor() >= 10) {
            final vest: Mdg_Vest = cast deathDropItem("mdg_vest", position);
            if (vest != null) {
                vest.armor = Math.round(player.Armor());
            }
        }
        if (player.playerData().hasHelmet) {
            deathDropItem("mdg_helmet", position);
        }
    }

    inline function deathDropItem(className: String, position: Vector): Null<Entity> {
        final ent = EntsLib.Create(className);
        if (Gmod.IsValid(ent)) {
            ent.SetPos(position + randomVectorVariance(0, 8));
            ent.SetAngles(randomVectorVariance(0, 1).Angle());
            ent.Spawn();
            return ent;
        }
        return null;
    }

    function randomVectorVariance(from: Float, to: Float): Vector {
        return new Vector(Random.float(from, to), Random.float(from, to), Random.float(from, to));
    }

    override function playerDeath(player: Player, inflictor: Entity, attacker: Entity) {
        final time = Gmod.CurTime();
        player.playerData().nextSpawnTime = time + 10;
        player.SetTeam(manager.TEAM_SPECTATORS);
        checkBattleDeath(player, "died");
    }

    override function playerDeathThink(player: Player): Bool {
        final spawnTime = player.playerData().nextSpawnTime;
        final time = Gmod.CurTime();
        if (spawnTime < time) {
            player.Spawn();
            return true;
        }
        return false;
    }

    override function playerJoined(player: Player) {}

    override function playerDisconnected(player: Player) {
        player.SetTeam(manager.TEAM_NONE);
        checkBattleDeath(player, "disconnected");
    }

    override function canPlayerSuicide(player: Player): Bool {
        return false;
    }
    #end
}
