![Deadthgrounds 2 logo](logo512.jpg)

# Masa's Deadthgrounds 2

[Deadthgrounds 2 in Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=2586334979)

## Requirements

- Haxe >= 4.2.4
- Haxelibs found in args.hxml. Gmodhaxe should use [latest source version](https://github.com/Dizbdeedee/gmodhaxe)
- [just](https://github.com/casey/just) used with a unix-like shell (you can also run the tasks manually by looking at the justfile)
- .env file with following variables:
    - `GMOD_PATH` pointing to Garry's Mod root path using path format that is resolvable by the above shell
- Asset content in the paths shown in justfile build step (extract content from the workshop addon)
- `gmodAddonFolder` option in args.hxml pointing to Garry's Mod addon folder

## License

See the [LICENSE file](LICENSE)
