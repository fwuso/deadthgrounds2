set dotenv-load

clean:
    rm -r dist

build: clean
    haxe build.hxml
    mkdir -p dist
    cp -r "$(echo "$GMOD_PATH"/garrysmod/addons/deadthgrounds2)" dist/
    cp -r "$(echo "$GMOD_PATH"/garrysmod/addons/mdg2_hbombs)"/* dist/deadthgrounds2
    cp -r "$(echo "$GMOD_PATH"/garrysmod/addons/mdg2_atow)"/* dist/deadthgrounds2
    cp -r "$(echo "$GMOD_PATH"/garrysmod/addons/mdg2_firesupport)"/* dist/deadthgrounds2
    eval '"$GMOD_PATH"/bin/gmad' create -folder dist/deadthgrounds2 -out dist/mdg2.gma

publish: build
    eval '"$GMOD_PATH"/bin/gmpublish' update -addon dist/mdg2.gma -id "2586334979" -changes "See https://codeberg.org/fwuso/deadthgrounds2/src/branch/main/CHANGELOG.md"
